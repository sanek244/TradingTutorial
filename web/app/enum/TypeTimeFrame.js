'use strict';

class TypeTimeFrame extends Enum{ }

TypeTimeFrame.DAY = 'days';
TypeTimeFrame.HOUR = 'hours';
TypeTimeFrame.MINUTES_15 = 'minutes15';

TypeTimeFrame.labels = {
    [TypeTimeFrame.DAY]: 'Дневной',
    [TypeTimeFrame.HOUR]: 'Часовик',
    [TypeTimeFrame.MINUTES_15]: '15-ти минутка',
};

TypeTimeFrame.times = {
    [TypeTimeFrame.DAY]: 1000*60*60*24,
    [TypeTimeFrame.HOUR]: 1000*60*60,
    [TypeTimeFrame.MINUTES_15]: 1000*60*15,
};


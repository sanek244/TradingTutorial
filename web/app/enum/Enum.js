'use strict';

/**
 * Перечисления
 */
class Enum{

    /**
     * Возвращает label по id
     * @param id {string}
     * @return {string}
     * @testOk
     */
    static getLabel(id) {
        return this.labels[id] || id;
    }
}

Enum.labels = {};


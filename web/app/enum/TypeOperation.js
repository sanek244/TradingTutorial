
class TypeOperation extends Enum{
    static getType(isBuy, oldCountPositions, newCountPositions){

        //start
        if(oldCountPositions === 0){
            return isBuy
                ? TypeOperation.LONG
                : TypeOperation.SHORT;
        }

        //end
        if(newCountPositions + oldCountPositions === 0){
            return TypeOperation.CLOSE;
        }

        //in trade
        return isBuy
            ? TypeOperation.BUY
            : TypeOperation.SELL;
    }
}

//start operation
TypeOperation.LONG = 'long';
TypeOperation.SHORT = 'short';

//end operation
TypeOperation.CLOSE = 'close';

//in trade
TypeOperation.BUY = 'buy';
TypeOperation.SELL = 'sell';

TypeOperation.labels = {
    [TypeOperation.LONG]: 'Длинная',
    [TypeOperation.SHORT]: 'Короткая',
    [TypeOperation.CLOSE]: 'Закрытие',
    [TypeOperation.BUY]: 'Покупка',
    [TypeOperation.SELL]: 'Продажа',
};

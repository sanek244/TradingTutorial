/**
 * Основной график для отображения акции
 */
class Chart{

    /**
     * @param {string} idChart id графика
     * @param {object} data Данные для графика
     * @param {*|string} name Название графика
     * @param {number} zoomX Откуда начинать просмотр графика (unixTIme)
     */
    constructor(idChart, data, name, zoomX){
        
        /**
         * Откуда начинать просмотр графика (unixTIme)
         * @type {number}
         */
        this.zoomX = zoomX;

        /**
         * Данные для графика
         * @type {object}
         */
        this._data = data;

        /**
         * График [Для масштабирования]
         * @type {null}
         */
        this.chart = null;

        /**
         * Название графика
         * @type {*|string}
         */
        this.name = name || '';

        /**
         * id графика
         * @type {string}
         */
        this.idChart = idChart;

        /**
         * Индикаторы key => true/false  = on/of
         * @type {{slidingLine50: boolean, slidingLine100: boolean, slidingLine200: boolean, volume: boolean, operationIndicators: boolean}}
         */
        this.seriesOn = {
            slidingLine50: true,
            slidingLine100: true,
            slidingLine200: true,
            volume: true,
            MACD: false,
            operationIndicators: false
        };

        this.drawChart = this.drawChart.bind(this);

        if($('#' + idChart).length){
            this.initZoom();
            this.initTheme();
            this.drawChart(idChart);
            this.zoomX = zoomX || this.chart.xAxis[0].min;
        }
    }


    //*** Свойства ***//
    /**
     * Данные графика
     * @param value
     */
    set data(value){
        this._data = value;
        this.drawChart(this.idChart);
    }

    get data(){
        return this._data;
    }



    //*** Методы ***//
    /**
     * Прорисовка графика
     * @param {string} id - id графика
     */
    drawChart(id){
        // подготовка данных - разделение на линии/бары графика
        let ohlc = [],
            volume = [],
            slidingLine50 = [],
            slidingLine100 = [],
            slidingLine200 = [],
            operations = [],
            MACD = [],
            MACDSignal = [],
            dataLength = this.data.length;

        let dataColumngLength = this.data.length > 0 ? this.data.last().length : 0;
        if(this.data.last() && this.data.last().last().length == 2){ //operations
            dataColumngLength--;
        }

        for (let i = 0; i < dataLength; i += 1) {
            ohlc.push([
                this.data[i][0], // the date
                this.data[i][1], // open
                this.data[i][2], // high
                this.data[i][3], // low
                this.data[i][4] // close
            ]);

            if(this.seriesOn.volume){
                volume.push([
                    this.data[i][0], // the date
                    this.data[i][5] // the volume
                ]);
            }

            if(this.seriesOn.slidingLine50 && dataColumngLength > 6){
                slidingLine50.push([
                    this.data[i][0], // the date
                    this.data[i][6]
                ]);
            }

            if(this.seriesOn.slidingLine100 && dataColumngLength > 7){
                slidingLine100.push([
                    this.data[i][0], // the date
                    this.data[i][7]
                ]);
            }

            if(this.seriesOn.slidingLine200 && dataColumngLength > 8){
                slidingLine200.push([
                    this.data[i][0], // the date
                    this.data[i][8]
                ]);
            }

            if(this.seriesOn.MACD && dataColumngLength > 10){
                MACD.push([
                    this.data[i][0], // the date
                    this.data[i][9]
                ]);

                MACDSignal.push([
                    this.data[i][0], // the date
                    this.data[i][10]
                ]);
            }


            if(this.seriesOn.operationIndicators){
                if(Array.isArray(this.data[i].last())){
                    operations.push({
                        x: this.data[i][0], // the date
                        title: this.data[i].last()[1],
                        text: this.data[i].last()[0]
                    });
                }
            }
        }

        const options = {
            title: {
                text: this.name
            },
            chart: {
                animation: false
            },
            rangeSelector: {
                inputEnabled: false,
                selected: 2,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1w'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'month',
                    count: 6,
                    text: '6m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }]
            },

            plotOptions: {
                series: {
                    stickyTracking: false
                }
            },
            tooltip: {
                crosshairs: [true, true],
                hideDelay: 100,
                shared: false,
            },
            xAxis: [
                {
                    events: {
                        afterSetExtremes: data => this.zoomX = data.min
                    }
                }
            ],

            yAxis: [
                {
                    labels: {
                        align: 'right',
                        x: -3
                    },
                    height: this.seriesOn.MACD ? '70%' : this.seriesOn.volume ? '90%' : '100%',
                    lineWidth: 2,
                    resize: {
                        enabled: true
                    }
                },
                this.seriesOn.volume ? {
                    min: 0,
                    labels: {
                        align: 'right',
                        x: -3
                    },
                    top: this.seriesOn.MACD ? '70%' : '85%',
                    height: '15%',
                    offset: 0,
                    lineWidth: 2
                } : null,
                this.seriesOn.MACD ? {
                    labels: {
                        align: 'right',
                        x: -3
                    },
                    top: '50%',
                    height: '50%',
                    offset: 0,
                    lineWidth: 2
                } : null
            ].filter(x => x),

            series: [
                {
                    type: 'candlestick',
                    data: ohlc,
                    name: this.name,
                    id: 'main'
                },
                this.seriesOn.volume ?
                    {
                        type: 'column',
                        name: 'Volume',
                        data: volume,
                        yAxis: 1,
                    } :  null,
                this.seriesOn.slidingLine50 ?
                    {
                        type: 'line',
                        name: 'MA50',
                        data: slidingLine50,
                    } : null,
                this.seriesOn.slidingLine100 ?
                    {
                        type: 'line',
                        name: 'MA100',
                        data: slidingLine100,
                    } : null,
                this.seriesOn.slidingLine200 ?
                    {
                        type: 'line',
                        name: 'MA200',
                        data: slidingLine200,
                    } : null,
                this.seriesOn.MACD ?
                    {
                        type: 'line',
                        name: 'MACD',
                        data: MACD,
                        yAxis: 1,
                    } :  null,
                this.seriesOn.MACD ?
                    {
                        type: 'line',
                        name: 'MACD SignalLine',
                        data: MACDSignal,
                        yAxis: 1,
                    } :  null,
                this.seriesOn.operationIndicators ?
                    {
                        type: 'flags',
                        name: 'buy/sell',
                        shape: 'squarepin',
                        onSeries: 'main',
                        data: operations,
                    } : null
            ].filter(x => x)
        };

        // create the chart
        this.chart = Highcharts.stockChart(id, options);
        this.chart.xAxis[0].setExtremes(this.zoomX, this.chart.xAxis[0].max);
    }

    /**
     * Инициализация стилей
     */
    initTheme(){
        Highcharts.theme = {
            colors: ['#0000ff', '#0000ff', '#ff0000', '#bdbfff', '#a5acff'],
        };

        Highcharts.setOptions(Highcharts.theme);
    }

    /**
     * Возврат прежнего уровня зума после пересборки графика
     */
    returnZoom(){
        if(this.chart.xAxis){
            this.chart.xAxis[0].setExtremes(this.zoomX, this.chart.xAxis[0].max);
        }
    }

    /**
     * Масштабирование колёсиком мыши
     */
    initZoom(){
        window.onWheel = intDelta => {
            this.zoomX = this.chart.xAxis[0].min + (this.chart.xAxis[0].max - this.chart.xAxis[0].min) / 4 * (intDelta > 0 ? -1 : 1);
            this.chart.xAxis[0].setExtremes(this.zoomX, this.chart.xAxis[0].max);
        };
    }
}





/**
 * Управление игровым процессом (покупка/продажа + движение графика)
 */
class Game extends StorageModel{

    init(){
        super.init();

        this.startMoney = 5000;

        /**
         * График
         * @type {Chart}
         */
        this.chart = null;

        /**
         * Все данные по текущей акции/бумаге
         */
        this.data = [];

        /**
         * Название текущей бумаги
         * @type {string}
         */
        this.stockName = '';

        /**
         * Последний показываемый бар данных
         * @type {number}
         */
        this.endBarIndex = 0;

        /**
         * Показываемый период на графике
         * @type {string}
         */
        this.showTimeFrame = TypeTimeFrame.DAY;

        /**
         * Текущий баланс
         * @type {number}
         * @private
         */
        this._balance = this.startMoney;
        /**
         * Количество имеющихся акций (если отрицательная величена, то short)
         * @type {number}
         * @private
         */
        this._countPositions = 0;
        /**
         * Текущий доход
         * @type {number}
         * @private
         */
        this._income = 0;

        /**
         * Текущая цена
         * @type {number}
         * @private
         */
        this._price = 0;

        /**
         * Текущая дата последнего бара в графике
         * @type {null}
         */
        this.dateChart = null;

        /**
         * История операций
         * @type {Operation[]}
         * @private
         */
        this._historyOperations = [];

        /**
         * Текст ошибки
         * @type {string}
         */
        this.errorText = '';

        /**
         * Дата начала игры
         * @type {Date}
         */
        this.dateBegin = null;

        /**
         * Максимальный баланс за игру
         * @type {number}
         */
        this.maxBalance = this.balance;
        /**
         * Минимальный баланс за игру
         * @type {number}
         */
        this.minBalance = this.balance;

        /**
         * Дата первого бара игры
         * @type {number}
         */
        this.dateFirstBar = -1;

        /**
         * Сохранённый zoomX из графика
         * @type {number}
         */
        this.lastZoomX = 0;

        this.buy = this.buy.bind(this);
        this.sell = this.sell.bind(this);
        this.next = this.next.bind(this);
        this.next10 = this.next10.bind(this);
        this.next30 = this.next30.bind(this);

        this.removeFielsForToObject.push('chart');
        this.removeFielsForToObject.push('dateChart');
        this.removeFielsForToObject.push('data');
    }

    afterInit(){
        super.afterInit(...arguments);

        this.refresh();
    }

    //*** Свойства ***//
    /**
     * Баланс - депозит
     * @return {number}
     */
    get balance(){
        return this._balance;
    }
    /**
     * Баланс - депозит
     * @param {number} value
     */
    set balance(value){
        value = parseFloat(value);
        ViewMain.instance.balance = value;
        this._balance = value;
    }


    /**
     * Количество позиций
     * @return {number}
     */
    get countPositions(){
        return this._countPositions;
    }
    /**
     * Количество позиций
     * @param {number} value
     */
    set countPositions(value){
        value = parseFloat(value);
        ViewMain.instance.countPositions = value;
        this._countPositions = value;
    }


    /**
     * Прибыль
     * @return {number}
     */
    get income(){
        return this._income;
    }
    /**
     * Прибыль
     * @param {number} value
     */
    set income(value){
        value = parseFloat(value);
        ViewMain.instance.income = value;
        this._income = value;
    }


    /**
     * Текущая цена
     * @return {number}
     */
    get price(){
        return this._price;
    }
    /**
     * Текущая цена
     * @param {number} value
     */
    set price(value){
        value = parseFloat(value);
        this._price = value;
        ViewMain.instance.price = value;
    }


    /**
     * История операций
     * @return {Operation[]}
     */
    get historyOperations() {
        return this._historyOperations;
    }
    /**
     * История операций
     * @param {Operation[]} value
     */
    set historyOperations(value){
        ViewMain.instance.history5lastOperations = value.slice().reverse();
        this._historyOperations = value;
    }

    /**
     * Возвращает данные по статистике игры
     * @return {StatisticsData}
     */
    get statisticsData(){
        return new StatisticsData(this);
    }


    //*** Методы ***//
    /**
     * Добавление новой операции
     * @param Operation {Operation}
     */
    addHistoryOperation(Operation){
        this.historyOperations.push(Operation);
        ViewMain.instance.history5lastOperations = this.historyOperations.slice().reverse();
    }

    /**
     * Обновление визуальной части
     */
    refresh(){
        this.showTimeFrame = this.showTimeFrame;
        this.chart = new Chart(ViewMain.instance.ids.mainChart, this.data.slice(0, this.endBarIndex), this.stockName, this.lastZoomX);

        //переопределение события клика
        ViewMain.instance.buttonBuyClick = this.buy.bind(this);
        ViewMain.instance.buttonSellClick = this.sell.bind(this);
        ViewMain.instance.buttonNextClick = this.next.bind(this);
        ViewMain.instance.buttonNext10Click = this.next10.bind(this);
        ViewMain.instance.buttonNext30Click = this.next30.bind(this);

        this.balance = this.balance;
        this.income = this.income;
        this.countPositions = this.countPositions;
        this.historyOperations = this.historyOperations;
        this.price = this.price;

        if(this.chart.data.length > 0){
            const lastBar = this.chart.data[this.chart.data.length - 1];
            this.price = lastBar[4];
            this.errorText = '';
            this.dateChart = new Date(lastBar[0]); // 0 - data in unixTime
            ViewMain.instance.priceChange = this.price - this.chart.data[this.chart.data.length - 2][4];
        }
    }

    /**
     * Покупка, закрытие short позиций
     */
    buy(){
        //check error
        if(!this.checkInputCount(TypeOperation.LONG)){
            ViewMain.instance.showMessage(this.errorText);
            return;
        }

        const operation = new Operation({
            operationType: TypeOperation.getType(true, this.countPositions, ViewMain.instance.inputCount),
            countPositions: ViewMain.instance.inputCount,
            price: this.price,
            dateChart: this.dateChart,
            indexChartBar: this.endBarIndex - 1,
            income: this.countPositions >= 0 ? 0 : this.income / (Math.abs(this.countPositions) / ViewMain.instance.inputCount),
        });

        if(this.countPositions < 0){
            let chageIncome = this.income / (Math.abs(this.countPositions) / ViewMain.instance.inputCount);
            this.income -= chageIncome;
            this.balance += ViewMain.instance.inputCount * this.price + chageIncome * 2;
            this.maxBalance = this.balance > this.maxBalance ? this.balance : this.maxBalance;
            this.minBalance = this.balance < this.minBalance ? this.balance : this.minBalance;
        }
        else{
            this.balance -= ViewMain.instance.inputCount * this.price;
        }
        this.countPositions += ViewMain.instance.inputCount;

        this.addHistoryOperation(operation);
        this.save();
    }

    /**
     * Продажа, открытие short позиции
     */
    sell(){
        //check error
        if(!this.checkInputCount(TypeOperation.SHORT)){
            ViewMain.instance.showMessage(this.errorText);
            return;
        }

        const operation = new Operation({
            operationType: TypeOperation.getType(false, this.countPositions, ViewMain.instance.inputCount * -1),
            countPositions: -ViewMain.instance.inputCount,
            price: this.price,
            dateChart: this.dateChart,
            indexChartBar: this.endBarIndex - 1,
            income: this.countPositions <= 0 ? 0 : this.income / (this.countPositions / ViewMain.instance.inputCount),
        });

        this.balance += ViewMain.instance.inputCount * this.price * (this.countPositions <= 0 ? -1 : 1);
        if(this.countPositions > 0){
            this.income -= this.income / (this.countPositions / ViewMain.instance.inputCount);
            this.maxBalance = this.balance > this.maxBalance ? this.balance : this.maxBalance;
            this.minBalance = this.balance < this.minBalance ? this.balance : this.minBalance;
        }
        this.countPositions -= ViewMain.instance.inputCount;

        this.addHistoryOperation(operation);
        this.save();
    }

    /**
     * Следующий бар
     */
    next(){
        if(this.endBarIndex < this.data.length - 1){
            this.endBarIndex++;
            this.chart.data = this.data.slice(0, this.endBarIndex);
            this.chart.zoomX += TypeTimeFrame.times[this.showTimeFrame];
            this.chart.returnZoom();

            const lastBar = this.chart.data[this.chart.data.length - 1];
            this.income += this.countPositions * (lastBar[4] - this.price);
            this.price = lastBar[4]; // 4 - close bar data
            this.dateChart = new Date(lastBar[0]); // 0 - data in unixTime
            this.save();
        }
        else {
            ViewMain.instance.showEndMessage('Данные по текущему графику закончились! Ознакомьтесь со своей статистикой.');
        }
    }

    /**
     * Следующие 10 баров
     */
    next10(){
        if(this.endBarIndex + 9 < this.data.length - 1){
            this.endBarIndex += 10;
            this.chart.data = this.data.slice(0, this.endBarIndex);
            this.chart.zoomX += TypeTimeFrame.times[this.showTimeFrame] * 15;
            this.chart.returnZoom();

            const lastBar = this.chart.data[this.chart.data.length - 1];
            this.income += this.countPositions * (lastBar[4] - this.price);
            this.price = lastBar[4]; // 4 - close bar data
            this.dateChart = new Date(lastBar[0]); // 0 - data in unixTime
            this.save();
        }
        else {
            ViewMain.instance.showEndMessage('Данные по текущему графику закончились! Ознакомьтесь со своей статистикой.');
        }
    }
    /**
     * Следующие 30 баров
     */
    next30(){
        if(this.endBarIndex + 29 < this.data.length - 1){
            this.endBarIndex += 30;
            this.chart.data = this.data.slice(0, this.endBarIndex);
            this.chart.zoomX += TypeTimeFrame.times[this.showTimeFrame] * 44;
            this.chart.returnZoom();

            const lastBar = this.chart.data[this.chart.data.length - 1];
            this.income += this.countPositions * (lastBar[4] - this.price);
            this.price = lastBar[4]; // 4 - close bar data
            this.dateChart = new Date(lastBar[0]); // 0 - data in unixTime
            this.save();
        }
        else {
            ViewMain.instance.showEndMessage('Данные по текущему графику закончились! Ознакомьтесь со своей статистикой.');
        }
    }

    /**
     * Проверка введённого значения в поле для количества
     * @param typeOperation {string}
     * @return {boolean}
     */
    checkInputCount(typeOperation){
        if(ViewMain.instance.inputCount <= 0){
            this.errorText = 'Количество позиций должно быть больше 0!';
            return false;
        }

        //long
        if(typeOperation === TypeOperation.LONG){
            //add
            if(this.countPositions >= 0){
                if(ViewMain.instance.inputCount > this.balance / this.price){
                    this.errorText = 'Не хватает ' + (this.price * ViewMain.instance.inputCount - this.balance) + '$.';
                    return false;
                }
                return true;
            }

            //close short
            if(ViewMain.instance.inputCount > Math.abs(this.countPositions)){
                this.errorText = 'Вы можете закрыть только ' + Math.abs(this.countPositions) + ' позиций.';
                return false;
            }
            return true;
        }

        if(typeOperation === TypeOperation.SHORT){
            //close long
            if(this.countPositions > 0){
                if(ViewMain.instance.inputCount > this.countPositions){
                    this.errorText = 'Вы можете закрыть только ' + this.countPositions + ' позиций.';
                    return false;
                }
                return true;
            }

            //add
            if(ViewMain.instance.inputCount > this.balance / this.price){
                this.errorText = 'Не хватает ' + (this.price * ViewMain.instance.inputCount - this.balance) + '$.';
                return false;
            }
            return true;
        }

        throw 'Незивестный тип операции';
    }


    /**
     * Перезапуск игры
     * @param {string} stockName - ключ акции из window.data
     * @param {string} timeFrame - играемый период из TypeTimeFrame
     * @param {Array} dataStock - данные акций по текущему интервалу
     */
    restart(stockName, timeFrame, dataStock) {
        this.data = dataStock;
        this.stockName = stockName;
        this.showTimeFrame = timeFrame;
        this.endBarIndex = Helper.getRandomInt(100, 300);
        this.dateFirstBar = this.data[this.endBarIndex - 1][0];
        this.dateBegin = new Date();

        this._balance = this.startMoney;
        this._countPositions = 0;
        this._income = 0;
        this._price = 0;
        this.dateChart = null;
        this._historyOperations = [];
        this.errorText = '';
        this.lastZoomX = dataStock[0][0];
        this.refresh();
        this.save();
    }

    /**
     * Загрузка данных
     */
    load(){
        if(super.load(...arguments)){
            this.refresh();

            if(this.stockName && this.showTimeFrame){
                ModalWait.instance.show('Загрузка данных...');
                ServiceController.getStockData(this.stockName, this.showTimeFrame).then(data => {
                    if(data.last().length <= 6){
                        Helper.addArithmeticMean(data);
                        //Helper.addMACD(data);
                    }
                    this.data = data;
                    this.refresh();
                })
            }
            return true;
        }

        return false;
    }

    /**
     * Сохранение данных
     */
    save(){
        this.lastZoomX = this.chart.zoomX;
        super.save();
    }


    /**
     * Возвращает сущность игры с загруженной последней сесии
     * @return {Game}
     */
    static loadLastGame(){
        const game = new Game();
        if(game.load()){
            return game;
        }
        return null;
    }


    /**
     * Возвращает максимально возможную прибыль от графика
     * @param {Array} data - уже упорядоченные данные
     * @param {number} rateFixed - фиксированная ставка и начальный баланс
     * @param {boolean} isFixedRate - ставка фиксированная? true - rateFixed, false - накопительная, на все деньги начиная с rateFixed
     * @param {boolean} is10percentRate - ставка на полную или на 10% ?
     * @param {boolean} returnAvrYear - вернуть среднее за год?
     */
    static getMaxProfit(data, rateFixed = 100, isFixedRate = true, is10percentRate = true, returnAvrYear = false){
        let profit = rateFixed;

        let rate, max, min;
        for(let i = 1; i < data.length; i++) {
            rate = (isFixedRate ? rateFixed : profit);
            if(is10percentRate){
                rate /= 10;
            }

            if(data[i - 1][4] > data[i][4]){
                max = data[i - 1][4];
                min = data[i][4];
            }
            else{
                max = data[i][4];
                min = data[i - 1][4];
            }

            profit += rate / min * max - rate;
        }
        profit -= rateFixed;

        if(returnAvrYear){
            return profit / Helper.betweenDateIntYear(data[data.length - 1][0], data[0][0]);
        }

        return profit;
    }

    /**
     * Возвращает максимально возможную прибыль по текущему графику(с тем же началом и концом и с тем же начальным депозитом)
     * @param {boolean} isFixedRate - ставка фиксированная? true - this.startMoney, false - накопительная, на все деньги начиная с this.startMoney
     * @param {boolean} is10percentRate - ставка на полную или на 10% ?
     */
    getMaxProfit(isFixedRate = true, is10percentRate = true){
        const data = this.data.slice(this.data.findIndex(x => x[0] === this.dateFirstBar), this.endBarIndex);
        return Game.getMaxProfit(data, this.startMoney, isFixedRate, is10percentRate);
    }
}

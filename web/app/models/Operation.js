class Operation extends BaseModel{
    init(){
        super.init();

        /**
         * Время и дата создания операции
         * @type {Date}
         */
        this.dataCreateOperation = new Date();

        /**
         * Дата по графику
         * @type {Date}
         */
        this.dateChart = null;

        /**
         * Количество взятого в операцию товара
         * @type {number}
         */
        this.countPositions = 0;

        /**
         * Тип операции
         * @type {TypeOperation}
         */
        this.operationType = '';

        /**
         * Цена
         * @type {number}
         */
        this.price = 0;

        /**
         * Доход от операции
         * @type {number}
         */
        this.income = 0;

        /**
         * Номер бара в данных графика
         * @type {number}
         */
        this.indexChartBar = 0;
    }
}

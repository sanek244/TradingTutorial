
class ChartCustom{
    constructor(idChart, data, name){
        /**
         * Данные для графика
         * @type {object}
         */
        this._data = data;

        /**
         * График [Для масштабирования]
         * @type {null}
         */
        this.chart = null;

        /**
         * Название графика
         * @type {*|string}
         */
        this.name = name || '';

        /**
         * id графика
         */
        this.idChart = idChart;


        this.drawChart = this.drawChart.bind(this);

        if($('#' + idChart).length){
            this.initZoom();
            this.initTheme();
            this.drawChart(idChart);
            this.zoomX = this.chart ? this.chart.xAxis[0].min : 0;
        }
    }


    //*** Свойства ***//
    /**
     * Данные графика
     * @param value
     */
    set data(value){
        this._data = value;
        this.drawChart(this.idChart);
    }

    get data(){
        return this._data;
    }



    //*** Методы ***//
    /**
     * Прорисовка графика
     * @param {string} id - id графика
     */
    drawChart(id){
        if(this.data.length === 0){
            return;
        }

        const volume = this.data[0]['data'].map(x => [x[0], x[5]]);
        const series = [
            {
                type: 'candlestick',
                data: this.data[0]['data'],
                name: this.name,
                id: 'main'
            },
            /*{
                type: 'column',
                name: 'Volume',
                data: volume,
                yAxis: 1,
            }*/
        ];
        let yAxis = [
            {
                labels: {
                    align: 'right',
                    x: -3
                },
                height: '70%',
                lineWidth: 2,
                resize: {
                    enabled: true
                }
            },
            /*{
                labels: {
                    align: 'right',
                    x: -3
                },
                top: '50%',
                height: '50%',
                lineWidth: 2
            }*/
        ];

        let iNewLineY = 1;
        this.data.map(x => {
            if('method' in x){
                switch(x['method']){
                    case 'operations':
                        series.push({
                            type: 'flags',
                            name: 'buy/sell',
                            shape: 'squarepin',
                            onSeries: 'main',
                            data: x['data'],
                        });
                        break;

                    case 'MACD':
                        series.push({
                            type: 'line',
                            name: x['name'],
                            data: x['data'][0],
                            yAxis: iNewLineY,
                        });
                        series.push({
                            type: 'area',
                            name: x['name'] + 'Signal',
                            data: x['data'][1],
                            yAxis: iNewLineY++,
                        });
                        yAxis.push({
                            labels: {
                                align: 'right',
                                x: -3
                            },
                            top: '55%',
                            height: '15%',
                            offset: 0,
                            lineWidth: 2
                        });
                        break;

                    case 'MA':
                        series.push({
                            type: 'line',
                            name: x['name'],
                            data: x['data'],
                        });
                        break;
                }
            }
        });
        const height = 100 / (iNewLineY + 3);
        yAxis[0]['height'] = height * 4 + '%';
        yAxis.map((el, i) => {
           if(i !== 0) {
               el['height'] = height + '%';
               el['top'] = height * 3 + height * i + '%'; //i = 1 volume
           }
        });

        const options = {
            title: {
                text: this.name
            },
            chart: {
                animation: false
            },
            rangeSelector: {
                inputEnabled: false,
                selected: 2,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1w'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'month',
                    count: 6,
                    text: '6m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }]
            },
            plotOptions: {
                series: {
                    stickyTracking: false
                }
            },
            tooltip: {
                crosshairs: [true, true],
                hideDelay: 100,
                shared: false,
            },
            xAxis: [
                {
                    events: {
                        afterSetExtremes: data => this.zoomX = data.min
                    }
                }
            ],
            yAxis: yAxis,
            series: series
        };

        // create the chart
        this.chart = Highcharts.stockChart(id, options);
        //this.chart.xAxis[0].setExtremes(0, this.data[0]['data'].last()[0]);
        this.chart.xAxis[0].setExtremes(this.chart.xAxis[0].min, this.chart.xAxis[0].max);
    }

    /**
     * Инициализация стилей
     */
    initTheme(){
        Highcharts.theme = {
            colors: ['#0000ff', '#0000ff', '#ff0000', '#bdbfff', '#a5acff'],
        };

        Highcharts.setOptions(Highcharts.theme);
    }

    returnZoom(){
        this.chart.xAxis[0].setExtremes(this.zoomX, this.chart.xAxis[0].max);
    }

    /**
     * Масштабирование колёсиком мыши
     */
    initZoom(){
        window.onWheel = intDelta => {
            this.zoomX = this.chart.xAxis[0].min + (this.chart.xAxis[0].max - this.chart.xAxis[0].min) / 4 * (intDelta > 0 ? -1 : 1);
            this.chart.xAxis[0].setExtremes(this.zoomX, this.chart.xAxis[0].max);
        };
    }
}





class Trade extends BaseModel{
    init(){
        super.init();

        /**
         * Операции трейда
         * @type {Operation[]}
         */
        this.operations = [];

        /**
         * @type {number}
         */
        this.id = -1;

        /**
         * Максимальная просадка
         * @type {number}
         */
        this.drawdown = 0;
    }

    /**
     * Доход за трейд
     */
    get income(){
        return parseFloat(this.operations.sum(x => x.income).toFixed(2));
    }
    /**
     * Доход за трейд (в процентах)
     */
    get incomePercent(){
        const startPosition = this.operations[0].price * Math.abs(this.operations[0].countPositions);
        return parseFloat(((startPosition + this.income) * 100 / startPosition - 100).toFixed(2));
    }

    /**
     * Время начала трейда по графику
     * @return {Date|null}
     */
    get startDate(){
        if(this.operations.length === 0){
            return null;
        }

        return Helper.getDate(this.operations[0].dateChart, true);
    }

    /**
     * Время окончания трейда по графику
     * @return {Date|null}
     */
    get endDate(){
        if(this.operations.length === 0){
            return null;
        }

        return Helper.parseDateTime(this.operations.last().dateChart);
    }

    /**
     * Время жизни трейда
     * @return {*}
     */
    get timeLife(){
        if(this.operations.length === 0){
            return '0ч.';
        }

        return Helper.betweenDate(this.endDate, this.startDate);
    }

    /**
     * Максимальное количество позиций
     * @return {number}
     */
    get maxCountPositions(){
        return parseFloat(this.operations[0].countPositions.toFixed(2));
    }

    /**
     * количество операций
     * @return {number}
     */
    get countOperations(){
        return this.operations.length;
    }


    /**
     * Просадка (в процентах)
     * @return {number}
     */
    get drawdownPercent(){
        if(this.operations.length === 0){
            return 0;
        }

        return parseFloat((this.drawdown * 100 / this.operations[0].price - 100).toFixed(2));
    }
}

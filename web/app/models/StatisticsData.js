class StatisticsData extends BaseModel{

    init(game){
        super.init();

        this.stockName = game.stockName;

        /**
         * Дата начала игры по графику
         * @type {string}
         */
        this.dataChartBegin = Helper.getDate(game.dateFirstBar, false, true);

        const dateChartCurrent = game.data[game.endBarIndex - 1][0];

        /**
         * Текущая дата игры по графику
         * @type {string}
         */
        this.dataChartCurrent = Helper.getDate(dateChartCurrent, false, true);

        /**
         * Время игры по графику
         * @type {string}
         */
        this.dataChartPeriod = Helper.betweenDate(game.dateFirstBar, dateChartCurrent);

        /**
         * Дата начала игры
         * @type {string}
         */
        this.dataBegin = Helper.getDate(game.dateBegin);

        /**
         * Дата окончания игры (время сохранения)
         * @type {string}
         */
        this.dateEnd = Date.now();

        /**
         * Время игры
         * @type {string}
         */
        this.dataPeriod = Helper.betweenDate(game.dateBegin, new Date());

        /**
         * Трейды
         */
        this.trades = this.getTrades(game.historyOperations, game.data);

        /**
         * Общая комиссия с операций
         */
        this.commissionSumAll = '-' + Helper.formatPrice(this.trades.sum(x => x.operations.sum(y => y.price * Math.abs(y.countPositions) * 0.001)).toFixed(2)) + '$';
        const sortTrades = this.trades.sortByField(x => x.income);
        const avrIncome = this.trades.avr(x => x.income);
        /**
         * Средняя прибыль
         * @type {string}
         */
        this.avrIncome = Helper.formatPrice(avrIncome.toFixed(2)) + '$';

        let avrIncomeMedian = 0;
        if(sortTrades.length === 1){
            avrIncomeMedian = sortTrades[0].income;
        }
        else if(sortTrades.length > 1){
            avrIncomeMedian = sortTrades[Math.round(sortTrades.length / 2)].income;
        }

        /**
         * Средняя прибыль по медиане
         * @type {string}
         */
        this.avrIncomeMedian = Helper.formatPrice(avrIncomeMedian.toFixed(2)) + '$';


        let avrProfitFromDay = 0;
        if(this.trades.length > 0){
            const startUnitTime = Helper.parseDateTime(this.trades[0].startDate).getTime();
            const endUnitTime = Helper.parseDateTime(this.trades.last().endDate).getTime();
            avrProfitFromDay = (this.trades.avr(x => x.income) / ((endUnitTime - startUnitTime) / 1000 / 60 / 60 / 24)).toFixed(2);
        }

        /**
         * Средняя прибыль на 1 день
         * @type {string}
         */
        this.avrIncomeOnDay = Helper.formatPrice(avrProfitFromDay) + '$';

        let timeLifePositions = this.trades.map(x => Helper.parseDateTime(x.endDate) - Helper.parseDateTime(x.startDate));
        /**
         * Среднее время жизни позиций
         * @type {string}
         */
        this.avrTimeLifePositions = Helper.betweenDate(new Date(0), timeLifePositions.avr());

        let bestTrade = '-';
        let failTrade = '-';
        if(this.trades.length > 0){
            bestTrade = sortTrades[sortTrades.length - 1].income;
            bestTrade = bestTrade <= 0 ? '-' : bestTrade.toFixed(2);
            failTrade = sortTrades[0].income;
            failTrade = failTrade >= 0 ? '-' : failTrade.toFixed(2);
        }

        /**
         * Самый лучший трейд
         * @type {string}
         */
        this.bestTrade = Helper.formatPrice(bestTrade) + (bestTrade === '-' ? '' : '$');

        /**
         * Самый худший трейд
         * @type {string}
         */
        this.failTrade = Helper.formatPrice(failTrade) + (failTrade === '-' ? '' : '$');

        const countBestTrades = sortTrades.count(x => x.income > 0);
        const countFailTrades = sortTrades.count(x => x.income < 0);
        const countZeroTrades = sortTrades.count(x => x.income === 0);
        /**
         * Соотношение трейдов (+/0/- = all)
         * @type {string}
         */
        this.ratioTrades = '<span class="limeText">' + countBestTrades + '</span>/' + countZeroTrades + '/<span class="redText">' + countFailTrades + '</span> = ' + this.trades.length;


        /**
         * Ихменение баланса со временем
         * @type {Array}
         */
        let balanceChange = game.startMoney;
        this.changeBalance = this.trades.map(trade => {
            balanceChange += trade.income;
            return parseInt(balanceChange);
        });

        /**
         * Пройдено пути %
         * @type {number}
         */
        this.percentTraversed = (game.endBarIndex / (game.data.length - 1) * 100).toFixed(1) + '%';

        const balance = game.balance + game.countPositions * game.price;
        /**
         * Конечный баланс
         * @type {number}
         */
        this.endBalance = Helper.formatPrice(parseInt(balance - game.startMoney)) + '$';

        /**
         * История операций
         * @type {Operation[]}
         * @private
         */
        this.historyOperations = game.historyOperations;
    }


    /**
     * трейды игры
     */
    getTrades(historyOperations, data){
        if(historyOperations.length === 0){
            return [];
        }

        let allTrades = [];
        let i = 0;
        let currentTrade = new Trade();

        historyOperations.map(operation => {
            currentTrade.operations.push(operation);
            currentTrade.id = i++;

            if(operation.operationType === TypeOperation.CLOSE) {
                allTrades.push(currentTrade);
                currentTrade = new Trade();
            }
        });

        //убираем все ложные (у оторых все операции были в один день проведены)
        allTrades = allTrades.filter(x => x.operations[0].dateChart !== x.operations[x.operations.length - 1].dateChart);

        //Ищем наибольшую просадку
        allTrades.map(trade => {
            //TODO: проверить trade.operations[0].indexChartBar === 0 ? 0 : trade.operations[0].indexChartBar - 1
            //При работе алгоритмов с 0 индекс не понятно
            const chartTrade = data.slice(trade.operations[0].indexChartBar === 0 ? 0 : trade.operations[0].indexChartBar - 1, trade.operations.last().indexChartBar);
            let chartTradeDrawdownBar = chartTrade.min(x => x[4]);
            if(trade.operations[0].operationType === TypeOperation.SHORT){
                chartTradeDrawdownBar = chartTrade.max(x => x[4]);
            }
            trade.drawdown = chartTradeDrawdownBar[4];
        });

        return allTrades;
    }
}

/**
 * Тексты надписей
 * @type {object}
 */
StatisticsData.labels = {
    labelDataChartBegin: 'Дата начала по графику',
    labelDataChartCurrent: 'Текущая дата по графику',
    labelDataChartPeriod: 'Время по графику',
    labelIncome: 'Прибыль',
    labelAvrIncome: 'Средняя прибыль',
    labelAvrIncomeMedian: 'Средняя прибыль по медиане',
    labelAvrIncomeOnDay: 'Средняя прибыль на 1 день',
    labelOldProgress: 'Прогресс от предыдущей игры',
    labelAvrTimeLifePositions: 'Среднее время жизни позиций',
    labelBestTrade: 'Самый лучший трейд',
    labelFailTrade: 'Самый худший трейд',
    labelRatioTrades: 'Соотношение трейдов (+/0/- = all)',
    labelCommissionSumAll: 'Суммарная комиссия',
};

/**
 * Класс связывающий сущность с localeStorage хранилищем
 */
class StorageModel extends BaseModel{


    //*** Функции ***//
    /**
     * Сохранение модели в localeStorage
     */
    save(){
        localStorage.setItem(this.className(), JSON.stringify(this.toObject()));
    }

    /**
     * Загрузка данных
     */
    load(){
        const data = localStorage.getItem(this.className());
        if(data){
            super.parse(JSON.parse(data));
            return true;
        }

        return false;
    }

    clear(){
        localStorage.removeItem(this.className());
    }
}

class BaseModel{

    constructor(){
        this.removeFielsForToObject = ['removeFielsForToObject'];

        this.init(...arguments);

        this.afterInit(...arguments);
    }

    /**
     * @protected
     * Кастомная Инициализация класса
     */
    init(){
    }

    /**
     * @protected
     * @param {object} data
     * @param {boolean} removeId
     * @testOk
     */
    afterInit(data){
        //загрузка данных
        this.parse(data);
    }

    //*** Функции ***//
    /**
     * Загрузка данных в модель
     * @param {object|number} data
     * @param {boolean} withErrors - выдавать ошибки при отсутствии полей в модели?
     * @testOk
     */
    parse(data, withErrors = false){
        if(data && typeof data === 'object'){
            Object.keys(data).map(key => {
                if(key in this) {
                    if(key[0] === '_'){
                        this[key.substr(1)] = data[key];
                    }
                    else{
                        this[key] = data[key];
                    }
                }
                else if(withErrors){
                    this.error('parse', 'Попытка записать "{0}" в отсутствующее поле: "{1}"'.format(data[key], key));
                }
            });
        }
    }

    /**
     * Вызов ошибки
     * @param {string} method
     * @param {string} message
     * @testOk
     */
    error(method, message){
        console.log('Ошибка: {0}. Метод "{1}", класс "{2}"'.format(message, method, this.className()));
    }

    /**
     * Возвращает объект экземпляра
     */
    toObject(){
        let res = {};

        Object.keys(this).map(key => {
            if(this.removeFielsForToObject.indexOf(key) === -1 && typeof this[key] !== 'function'){
                if(typeof this[key] === 'object' && this[key])
                {
                    if('toObject' in this[key]){
                        res[key] = this[key].toObject();
                    }
                    else if('length' in this[key] && this[key].length > 0 && typeof this[key][0] === 'object' && 'toObject' in this[key][0]){
                        const arrayObjects = [];
                        this[key].map(el => {
                            arrayObjects.push(el.toObject());
                        });
                        res[key] = arrayObjects;
                    }
                    else{
                        res[key] = this[key];
                    }
                }
                else{
                    //Приведение к типу
                    let value = this[key] + '';
                    if(parseInt(value) + '' === value){
                        res[key] = parseInt(value);
                        return;
                    }

                    //new RegExp("0*$") - remove '0' right
                    value = (this[key] + '').replace(',', '.') + '1';
                    if(this[key] !== '' && parseFloat(value) + '' === value){
                        res[key] = parseFloat(value.substr(0, value.length - 1));
                        return;
                    }

                    if(this[key] === 'true'){
                        res[key] = true;
                        return;
                    }

                    if(this[key] === 'false'){
                        res[key] = false;
                        return;
                    }

                    res[key] = this[key];
                }
            }

        });

        return res;
    }

    /**
     * Return class name
     * @returns {string}
     * @testOk
     */
    className() {
        return this.constructor.name;
    }

    /**
     * Return full class
     * @returns {string}
     * @testOk
     */
    static className() {
        return this.name;
    }
}

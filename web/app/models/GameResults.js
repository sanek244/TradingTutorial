/**
 * Все сохранённые результаты игр
 */
class GameResults extends StorageModel{

    init(){
        super.init();

        /** 
         * Все результаты
         * @type {StatisticsData[]}
        */
        this.results = [];

        /** 
         * Дата сохранения модели (unixTime) 
         * @type {number}
        */
        this.dateSave = 0; 

        this.load();
    }

    /**
     * Сохранить новую игру
     * @param {StatisticsData} statisticsData 
     */
    static save(statisticsData){
        let x = new GameResults();
        x.results.push(statisticsData);
        x.save();
    }

    save(){
        this.dateSave = Date.now(); 
        super.save();
    }

    /**
     * Возвращяет все результаты
     */
    static getResults(){
        let x = new GameResults();
        return x.results;
    }
}

'use strict';

/**
 * Статический класс контроллер с запросами к сервисам
 */
class ServiceController {

    //*** GET ***//
    /**
     * Получение названий бумаг/акций/валют
     * @return {Promise} параметр: array - данные от сервиса
     */
    static getStockNames() {
        return this.sendCoorsQuery('GET', ServiceController.host + '/api/v1/Data/Names', null, 'Получение названий бумаг');
    }

    /**
     * Получение данных бумаг/акций/валют по указанному периуду
     * @param {string} stockName - название бумаги/акции/валюты
     * @param {string} timeFrame - период
     * @return {Promise} параметр: array - данные от сервиса
     */
    static getStockData(stockName, timeFrame) {
        return this.sendCoorsQuery('GET', ServiceController.host + '/api/v1/Data/Stock?stockName={0}&timeFrame={1}'.format(stockName, timeFrame), null, 'Получение данных бумаги');
    }


    /**
     * Запрос к сервису
     * @param {string} method - POST или GET
     * @param {string} url - url сервиса
     * @param {object} data - передаваемые данные
     * @param {string} nameService - название сервиса
     * @param {string} responseType - тип ответа
     * @return {Promise} параметр: object - данные от сервиса
     */
    static sendCoorsQuery(method, url, data, nameService = '', responseType = 'json') {
        return new Promise((resolve, reject) => {
            const XHR = ('onload' in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
            const xhr = new XHR();

            if (responseType !== null) {
                xhr.responseType = responseType;
            }

            xhr.open(method, url, true);

            xhr.onload = () => {
                ModalWait.instance.hide();
                if (xhr.status === 200) {
                    if (!xhr.responseType || xhr.responseType === "text") {
                        resolve(xhr.responseText);
                    }
                    else if (xhr.responseType === 'json') {
                        resolve(xhr.response);
                    }
                    else if (xhr.responseType === "document") {
                        resolve(xhr.responseXML);
                    }
                    else {
                        resolve(xhr.response);
                    }
                }
                else {
                    if (!xhr.response) {
                        console.log('С сервисом "{0}" возникли проблемы. url: "{1}"'.format(nameService, url));
                        ModalAlert.instance.show('С сервисом "{0}" возникли проблемы!'.format(nameService));
                    }

                    reject(xhr.response);
                }
            };

            xhr.onerror = () => {
                ModalWait.instance.hide();
                console.log('Сервис "{0}" не отвечает. url: "{1}"'.format(nameService, url));
                ModalAlert.instance.show('Сервис "{0}" не отвечает!'.format(nameService));
                reject();
            };

            if (data) {
                xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
                xhr.send(JSON.stringify(data));
            }
            else {
                xhr.send();
            }
        });
    }
}
ServiceController.host = 'http://localhost:7004';


class Helper{

    static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static randomItem(object){
        const keys = Object.keys(object);
        const keySelected = keys[Helper.getRandomInt(0, keys.length)];
        return object[keySelected];
    }

    static randomKey(object){
        const keys = Object.keys(object);
        return keys[Helper.getRandomInt(0, keys.length)];
    }

    /**
     * Парсит строку даты в тип JS Date
     * @param {string|Date} dateTime
     * @return {Date}
     */
    static parseDateTime(dateTime){
        const d =  new Date(dateTime);
        if(Date.parse(dateTime) || !isNaN(d.getTime())){
            return new Date(dateTime);
        }

        if(dateTime === null || dateTime === 'null' || dateTime === '' || !dateTime){
            return null;
        }

        const dateJs = new Date();

        const dateTimePart = dateTime.split(' ');
        let datePart = null;

        //ищем разделительный знак
        for(let i = 1; i < dateTimePart[0].length - 1; i++){
            if(parseInt(dateTimePart[0][i]) + '' !== dateTimePart[0][i]){
                datePart = dateTimePart[0].split(dateTimePart[0][i]);
                break;
            }
        }

        //set date
        if((datePart[0] + '').length === 4){
            dateJs.setYear(datePart[0]);
            dateJs.setMonth(parseInt(datePart[1]) - 1);
            dateJs.setDate(datePart[2]);
        }
        else {
            let year = datePart[2];
            if(year.length === 2){
                year = (new Date()).getYear().toString().substr(0, 2) + year;
            }
            dateJs.setYear(year);
            dateJs.setMonth(parseInt(datePart[1]) - 1);
            dateJs.setDate(datePart[0]);
        }

        //set time
        if(dateTimePart[1]){
            const timePart = dateTimePart[1].split(':');
            timePart[timePart.length - 1] = timePart[timePart.length - 1].split(' ')[0];
            dateJs.setHours(timePart[0]);
            dateJs.setMinutes(timePart[1]);
            if(timePart.length > 2){
                dateJs.setSeconds(timePart[2]);
            }
        }

        return dateJs;
    }

    /**
     * Получить текущее время в формате y.m.d h:i:s
     * @param {string|Date|number} dateTime - время и дата
     * @param {boolean} isWithoutUTC - без часового пояса?
     * @param {boolean} withoutTime - без часов и минут?
     * @return {string}
     * @testOk
     */
    static getDate(dateTime = null, isWithoutUTC = false, withoutTime = false) {
        let t = new Date();
        if(dateTime){
            t = Helper.parseDateTime(dateTime);
        }

        if(!t){
            return '';
        }

        if(isWithoutUTC){
            let date = this.addZeroData(t.getUTCDate()) + '.' + this.addZeroData(t.getUTCMonth() + 1) + '.' + t.getUTCFullYear();
            return withoutTime 
                ? date
                : date + ' ' + this.addZeroData(t.getUTCHours()) + ':' + this.addZeroData(t.getUTCMinutes()) + ':' + this.addZeroData(t.getUTCSeconds());
        }

        let date = this.addZeroData(t.getDate()) + '.' + this.addZeroData(t.getMonth() + 1) + '.' + t.getFullYear();
        return withoutTime 
            ? date
            : date + ' ' + this.addZeroData(t.getHours()) + ':' + this.addZeroData(t.getMinutes()) + ':' + this.addZeroData(t.getSeconds());
    }

    /**
     * Разница между датами
     * @param {string|Date|number} date1
     * @param {string|Date|number} date2
     * @return {string}
     */
    static betweenDate(date1, date2){
        let maxDate = this.parseDateTime(date1);
        let minDate = this.parseDateTime(date2);

        if(minDate > maxDate){
            let tempDate = maxDate;
            maxDate = minDate;
            minDate = tempDate;
        }
        const newDate = new Date(maxDate - minDate);
        const dateInit = new Date(0);

        let res = '';

        //year
        const difYears = newDate.getUTCFullYear() - dateInit.getUTCFullYear();
        if(difYears){
            res += difYears + (difYears % 10 > 4 || difYears % 10 == 0 || difYears % 100 <= 20 && difYears % 100 > 4 ? 'лет ' : 'г. ');
        }

        //month
        const difMonths = newDate.getUTCMonth() - dateInit.getUTCMonth();
        if(difMonths){
            res += difMonths + 'мес. ';
        }

        //day
        const difDays = newDate.getUTCDate() - dateInit.getUTCDate();
        if(difDays){
            res += difDays + 'дн. ';
        }

        //hour
        const difHours = newDate.getUTCHours() - dateInit.getUTCHours();
        if(difHours){
            res += difHours + 'ч. ';
        }

        //minutes
        const difMinutes = newDate.getUTCMinutes() - dateInit.getUTCMinutes();
        if(difMinutes){
            res += difMinutes + 'м. ';
        }

        return res;
    }

    /**
     * Разница между датами в годах
     * @param {string|Date|number} date1
     * @param {string|Date|number} date2
     * @return {number}
     */
    static betweenDateIntYear(date1, date2){
        let maxDate = this.parseDateTime(date1);
        let minDate = this.parseDateTime(date2);

        if(minDate > maxDate){
            let tempDate = maxDate;
            maxDate = minDate;
            minDate = tempDate;
        }
        const newDate = new Date(maxDate - minDate);
        const dateInit = new Date(0);

        let res = 0;

        //year
        const difYears = newDate.getUTCFullYear() - dateInit.getUTCFullYear();
        if(difYears){
            res += difYears;
        }

        //month
        const difMonths = newDate.getUTCMonth() - dateInit.getUTCMonth();
        if(difMonths){
            res += 1 / (12 / difMonths);
        }

        //day
        const difDays = newDate.getUTCDate() - dateInit.getUTCDate();
        if(difDays){
            res += 1 / (30 / difDays) / 12;
        }

        return res;
    }

    /**
     * Формат числа xx (для даты)
     * @param i
     * @return {string}
     * @testOk
     */
    static addZeroData(i) {
        return i < 10 ? '0' + i : i;
    }

    /**
     * Откругляет число до такого же количества знаков, что и в цене
     * @param {number} number
     * @param {number} price
     */
    static roundToPrice(number, price){
        const priceStr = (price + '').replace(',', '.').trim();
        let indexComma = priceStr.indexOf('.');

        let res = '';
        if(indexComma !== -1){
            res = parseFloat(number).toFixed(5) + '';
        }
        else{
            res = parseFloat(number).toFixed(priceStr.length - indexComma) + '';
        }

        while(res[res.length - 1] === '0'){
            res = res.substr(0, res.length - 1);
        }

        if(res[res.length - 1] === '.'){
            res = res.substr(0, res.length - 1);
        }

        return res;
    }

    /**
     * Uid
     * @return {string}
     * @testOk
     */
    static generateUid(){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            let r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    /**
     * Глубокое клонирование
     * @param el
     */
    static cloneDeep(el){
        if(el && typeof el === 'object'){
            if('length' in el){
                return el.cloneDeep();
            }

            const newObject = {};

            Object.keys(el).map(key => {
                if (el[key] && typeof el[key] === 'object') {
                    newObject[key] = el[key].cloneDeep();
                }
                else {
                    newObject[key] = el[key];
                }
            });

            return newObject;
        }

        return el;
    }

    
    /**
     * Clone deep
     * @param {any} object
     * @returns {any}
     */
    static clone(object){
        return JSON.parse(JSON.stringify(object));
    }

    
    /**
     * Добавляет пробелы между тремя символами (для удобства просмотра больших чисел)
     * @param {any} price 
     * @returns {string}
     */
    static formatPrice(price){
        price = price + '';
        for(var i = (price.indexOf('.') > -1 ? price.indexOf('.') : price.length) - 3; i > 0; i -= 3)
            price = price.insert(i, ' ');
        return price;
    }

    

    /**
     * Добавляет среднеарифметическую скользящую
     */
    static addArithmeticMean(data){
        return data
            .sort((a, b) => {
                if(a[0] > b[0]){
                    return 1;
                }
                if(a[0] < b[0]){
                    return -1;
                }
                return 0;
            })
            .map((el, index, array) => {
            //среднеарифметическая скользящая
            let sum = 0,
                sum50 = 0,
                sum100 = 0,
                sum200 = 0;
            for(let i = index; i > index - 200; i--){
                sum += i > -1 ? array[i][4] : array[0][4];

                if(i === index - 50 + 1){
                    sum50 = sum;
                }
                if(i === index - 100 + 1){
                    sum100 = sum;
                }
            }
            sum200 = sum;

            //общая скользящая с весовыми коэффициентами
            let sumKof50 = 0,
                sumKof100 = 0,
                sumKof200 = 0;
            for(let i = index; i > index - 200; i--){
                const elValue = Math.pow(i > -1 ? array[i][4] : array[0][4], 2);
                if(i > index - 50){
                    sumKof50 += elValue / sum50;
                }
                if(i > index - 100){
                    sumKof100 += elValue / sum100;
                }
                sumKof200 += elValue / sum200;
            }

            let kofFixed = (el[1] + '').length - (el[1] + '').indexOf('.');
            el.push(sumKof50);
            el.push(sumKof100);
            el.push(sumKof200);

            return el;
        });
    }

    /**
     * Добавляет MACD индикатор
     */
    static addMACD(data){
        //macd - need to add: const macd = require('ta-lib.macd'); //"ta-lib.macd": "0.0.4"
        const macdRes = macd(data.map(el => el[4]), 4, 34, 4);
        return data.map((el, i) => {
            el.push(macdRes['macd'][i]);
            el.push(macdRes['signalLine'][i]);
            return el;
        });
    }
}

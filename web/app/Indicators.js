class Indicators{

    /* Функция, которая вычисляет MACD (Moving Average Convergance-Divergence).
     *
     * @param yData: массив переменных y.
     * @param xData: массив переменных x.
     * @param периоды: количество «дней» в среднем от.
     * @return: массив с 3 массивами. (0: macd, 1: signalline, 2: гистограмма)
    **/
    static MACD (xData, yData, params = [1, 1, 1]) {

        let shortEMA,
            longEMA,
            MACD = [],
            xMACD = [],
            yMACD = [],
            signalLine = [],
            histogram = [],
            shortPeriod = params[0],
            longPeriod = params[1],
            signalPeriod = params[2];

        // Calculating the short and long EMA used when calculating the MACD
        shortEMA = Indicators.MA(xData, yData, [shortPeriod]);
        longEMA = Indicators.MA(xData, yData, [longPeriod]);

        // subtract each Y value from the EMA's and create the new dataset (MACD)
        for (let i = 0; i < shortEMA.length; i++) {

            if (longEMA[i][1] === null) {
                MACD.push( [xData[i] , null]);
            } else {
                MACD.push( [ xData[i] , (shortEMA[i][1] - longEMA[i][1]) ] );
            }
        }

        // Set the Y and X data of the MACD. This is used in calculating the signal line.
        for (let i = 0; i < MACD.length; i++) {
            xMACD.push(MACD[i][0]);
            yMACD.push(MACD[i][1]);
        }

        // Setting the signalline (Signal Line: X-day EMA of MACD line).
        signalLine = Indicators.MA(xMACD, yMACD, [signalPeriod]);

        // Setting the MACD Histogram. In comparison to the loop with pure MACD this loop uses MACD x value not xData.
        for (let i = 0; i < MACD.length; i++) {

            if (MACD[i][1] === null) {

                histogram.push( [ MACD[i][0], null ] );

            } else {

                histogram.push( [ MACD[i][0], (MACD[i][1] - signalLine[i][1]) ] );

            }
        }

        return [MACD, signalLine, histogram];
    }


  /* Функция, основанная на идее экспоненциального скользящего среднего.
     *
     * Формула: EMA = цена (t) * k + EMA (y) * (1 - k)
     * t = сегодня, y = вчера, N = количество дней в EMA, k = 2 / (2N + 1)
     *
     * @param yData: массив переменных y.
     * @param xData: массив переменных x.
     * @param периоды: количество «дней» в среднем от.
     * @ вернуть массив, содержащий EMA.
    **/
    static MA (xData, yData, params) {

        let t,
            y = false,
            n = params[0],
            k = (2 / (n + 1)),
            ema,	// Экспоненциальная скользящая средняя.
            emLine = [],
            periodArr = [],
            length = yData.length,
            pointStart = xData[0];

       // циклические данные
        for (let i = 0; i < length; i++) {


            // Добавить последнюю точку в период arr, но только если ее набор.
            if (yData[i-1]) {
                periodArr.push(yData[i]);
            }


          // 0: выполняется, если для параметра periodArr достаточно точек.
            // 1: установить currentvalue (сегодня).
            // 2: установить последнее значение. либо прошлым средним, либо вчерашним ema.
            // 3: вычислять сегодняшние ema.
            if (n === periodArr.length) {


                t = yData[i];

                if (!y) {
                    y = Indicators.arrayAvg(periodArr);
                } else {
                    ema = (t * k) + (y * (1 - k));
                    y = ema;
                }

                emLine.push([xData[i] , y]);

               // удалить первое значение в массиве.
                periodArr.splice(0,1);

            } else {

                emLine.push([xData[i] , null]);
            }

        }

        return emLine;
    }


    /* Функция, которая возвращает среднее значение массива.
     *
    **/
    static arrayAvg (arr) {
        let sum = 0,
            arrLength = arr.length,
            i = arrLength;

        while (i--) {
            sum = sum + arr[i];
        }

        return (sum / arrLength);
    }
}


'use strict';

/**
 * Статический класс контроллер с запросами к сервисам
 */
class ServiceController {

    //*** GET ***//
    /**
     * Получение названий бумаг/акций/валют
     * @return {Promise} параметр: array - данные от сервиса
     */
    static getStockNames() {
        return new Promise(resolveOuter => {
            resolveOuter({
                "AAPL":["days"],
                "AMD":["days"],
                "AMZN":["days"],
                "GOOGL":["days"],
                "INTC":["days"],
                "MSFT":["days"],
                "NFLX":["days"],
                "NVDA":["days"],
                "YNDX":["days"],
    
                "BTCUSD":["days","hours","minutes15"],
                "DSHUSD":["days","hours","minutes15"],
                "ETHUSD":["days","hours","minutes15"],
                "XRPUSD":["days","hours","minutes15"],
                "LTCUSD":["days","hours","minutes15"],
    
                "AUDUSD":["days","hours","minutes15"],
                "EURUSD":["days","hours","minutes15"]
            })
        });
    }

    /**
     * Получение данных бумаг/акций/валют по указанному периуду
     * @param {string} stockName - название бумаги/акции/валюты
     * @param {string} timeFrame - период
     * @return {Promise} параметр: array - данные от сервиса
     */
    static getStockData(stockName, timeFrame) {
        var timeFramePart = timeFrame == 'days' 
            ? '1D'
            : timeFrame == 'hours' 
                ? '1h'
                : '15m';
        return this.sendCoorsQuery('GET', '/data/{0}/{1}_{2}.json'.format(timeFrame, stockName, timeFramePart), null, 'Получение данных бумаги');
    }


    /**
     * Запрос к сервису
     * @param {string} method - POST или GET
     * @param {string} url - url сервиса
     * @param {object} data - передаваемые данные
     * @param {string} nameService - название сервиса
     * @param {string} responseType - тип ответа
     * @return {Promise} параметр: object - данные от сервиса
     */
    static sendCoorsQuery(method, url, data, nameService = '', responseType = 'json') {
        return new Promise((resolve, reject) => {
            const XHR = ('onload' in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
            const xhr = new XHR();

            if (responseType !== null) {
                xhr.responseType = responseType;
            }

            xhr.open(method, url, true);

            xhr.onload = () => {
                ModalWait.instance.hide();
                if (xhr.status === 200) {
                    if (!xhr.responseType || xhr.responseType === "text") {
                        resolve(xhr.responseText);
                    }
                    else if (xhr.responseType === 'json') {
                        resolve(xhr.response);
                    }
                    else if (xhr.responseType === "document") {
                        resolve(xhr.responseXML);
                    }
                    else {
                        resolve(xhr.response);
                    }
                }
                else {
                    if (!xhr.response) {
                        console.log('С сервисом "{0}" возникли проблемы. url: "{1}"'.format(nameService, url));
                        ModalAlert.instance.show('С сервисом "{0}" возникли проблемы!'.format(nameService));
                    }

                    reject(xhr.response);
                }
            };

            xhr.onerror = () => {
                ModalWait.instance.hide();
                console.log('Сервис "{0}" не отвечает. url: "{1}"'.format(nameService, url));
                ModalAlert.instance.show('Сервис "{0}" не отвечает!'.format(nameService));
                reject();
            };

            if (data) {
                xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
                xhr.send(JSON.stringify(data));
            }
            else {
                xhr.send();
            }
        });
    }
}


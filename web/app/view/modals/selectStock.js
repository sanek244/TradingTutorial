class ModalSelectStock extends BaseView{
    init(){
        super.init();

        this.stocks = {};
        this.ids = {
            selectStock: this.idEl + '__select-stock',
            selectTimeFrame: this.idEl + '__select-timeFrame',
        };

        this.onChangeSelectStock = this.onChangeSelectStock.bind(this);
        this.start = this.start.bind(this);
        this.randomGame = this.randomGame.bind(this);
    }

    show(){
        super.show();

        ServiceController.getStockNames().then(data => {
            this.stocks = data;
            $$(this.ids.selectStock).config.options = Object.keys(data).sort();
            $$(this.ids.selectStock).refresh();
            this.onChangeSelectStock(Object.keys(this.stocks)[0]);
        });
    }

    hide(){
        super.hide();
        ModalMenu.instance.show();
    }

    randomGame(){
        const randomKeyStock = Helper.randomKey(this.stocks);
        let stockTimeFrames = this.stocks[randomKeyStock];
        stockTimeFrames = stockTimeFrames[Helper.getRandomInt(0, stockTimeFrames.length)];
        this._run(randomKeyStock, stockTimeFrames);
    }

    start(){
        this._run();
    }
    _run(keyStock, timeFrame){
        keyStock = keyStock || $$(this.ids.selectStock).getValue();
        timeFrame = timeFrame || $$(this.ids.selectTimeFrame).getValue();

        ModalWait.instance.show('Загрузка данных...');
        ServiceController.getStockData(keyStock, timeFrame).then(data => {
            if(data.last().length <= 6){
                Helper.addArithmeticMean(data);
                //Helper.addMACD(data);
            }
            if(ViewMain.instance.game === null){
                ViewMain.instance.game = new Game();
            }
            ViewMain.instance.game.restart(keyStock, timeFrame, data);
            this.hide();
            ModalMenu.instance.hide();
        });
    }

    onChangeSelectStock(value){
        //timeFrames
        $$(this.ids.selectTimeFrame).config.options =
            this.stocks[value]
                .sort()
                .map(el => {
                    return {id: el, value: TypeTimeFrame.getLabel(el)}
                });
        $$(this.ids.selectTimeFrame).refresh();
    }
}
ModalSelectStock.instance = new ModalSelectStock();

webix.ready(() => {
    const model = ModalSelectStock.instance;

    webix.ui(WidgetModal.template(model, 'Выбор ценной бумаги', 300, 300, 550, 200, {
        paddingX: 30,
        rows: [
            {view: 'label', label: 'Акции, Валютные пары, Крипта'},
            {
                view: 'select',
                value: 0,
                id: model.ids.selectStock,
                options: [],
                on: {
                    onChange: model.onChangeSelectStock
                }
            },
            {
                view: 'select',
                id: model.ids.selectTimeFrame,
                label: 'Интервал',
                value: TypeTimeFrame.DAY,
                options:[
                    { id: TypeTimeFrame.DAY, value: TypeTimeFrame.getLabel(TypeTimeFrame.DAY)},
                    { id: TypeTimeFrame.HOUR, value: TypeTimeFrame.getLabel(TypeTimeFrame.HOUR) },
                    { id: TypeTimeFrame.MINUTES_15, value: TypeTimeFrame.getLabel(TypeTimeFrame.MINUTES_15) }
                ]
            },
            {},
            {view: 'button', label: 'Запуск', click: model.start},
            {},
            {view: 'button', label: 'Рандом', click: model.randomGame},
            {},
            {},
            {view: 'button', label: 'Назад', click: model.hide},
            {},
        ]
    }));
});

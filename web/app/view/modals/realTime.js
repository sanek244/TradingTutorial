class ModalRealTime extends BaseView{
    init(){
        super.init();

        this.ids = {
            labelDataChartBegin: this.idEl + '__date-chart-begin',
            openChartTrades: this.idEl + '__open-chart-trades',
            tableOperations: this.idEl + '__table-operations',
            labelBalanceInUSD: this.idEl + '__label-balance-in-USD',
            labelPositions: this.idEl + '__label-positions',
            labelIncomeDrawdown: this.idEl + '__label-income-drawdown',
            labelTimeWork: this.idEl + '__label-time-work',
            labelIncome: this.idEl + '__label-income',
        };

        /**
         *
         * @type {object}
         */
        this.res = null;


        this.openChartOperations = this.openChartOperations.bind(this);
        this.onResize = this.onResize.bind(this);
        $(window).resize(this.onResize);
    }


    /**
     * @param {object} res
     */
    show(res){
        super.show();

        this.res = res;
        this.setValueLabel(this.ids.labelBalanceInUSD, res.realData.balanceImUSD);
        this.setValueLabel(this.ids.labelPositions, res.realData.positions);
        //this.setValueLabel(this.ids.labelIncomeDrawdown, res.realData.incomeDrawdown, true);
        this.setValueLabel(this.ids.labelTimeWork, res.realData.timeWork);
        this.setValueLabel(this.ids.labelIncome, res.realData.income, true);

        $$(this.ids.tableOperations).clearAll();
        $$(this.ids.tableOperations).parse(res.operations);
        $$(this.ids.tableOperations).refresh();
    }

    setValueLabel(id, valueLabel, isOnColor = false){
        let cssValue = 'blackText';
        valueLabel = valueLabel + '';
        const number = valueLabel ? parseFloat(valueLabel.substring(0, valueLabel.length -1)) : 0;
        if(isOnColor && valueLabel && valueLabel.substring(0, valueLabel.length -1) === number + ''){
            if(number > 0){
                cssValue = 'limeText boldText'
            }

            if(number < 0){
                cssValue = 'redText boldText';
            }
        }

        const idLabel = Object.keys(this.ids).filter(x => this.ids[x] === id)[0];

        $$(id).setHTML(
            '<span class="blueText">' +
            ModalRealTime.labels[idLabel] +
            '</span>: ' +
            '<span class="' + cssValue + '">' +
            valueLabel +
            '</span>'
        );
    }

    openChartOperations(){
        ModalChartTrades.instance.showOperations(this.res.operations);
    }

    onResize(){
        if(this.el.isVisible()){
            const body = $('body');
            const width = body.width();
            const height = body.height();
            this.el.$setSize(width, height);
        }
    }

    key(){
        //keys
        $(document).on('keydown', (event) => {
            if(this.el && this.el.isVisible() && !ModalChartTrade.instance.el.isVisible()) {
                switch (event.keyCode) {
                    case 27: this.hide(); break; //ESC
                }

                if(event.keyCode === 27){
                    event.preventDefault();
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                }
            }
        });
    }
}
/**
 * Тексты надписей
 * @type {object}
 */
ModalRealTime.labels = {
    labelBalanceInUSD: 'Баланс в USD',
    labelPositions: 'Позиции',
    labelIncomeDrawdown: 'Текущий доход/убыток от позиций',
    labelTimeWork: 'Время работы бота',
    labelIncome: 'Заработано',
};
ModalRealTime.instance = new ModalRealTime();

webix.ready(() => {
    const model = ModalRealTime.instance;
    const body = $('body');
    const width = body.width();
    const height = body.height();
    const heightLabel = 30;

    webix.ui({
        view: 'window',
        modal: true,
        id: model.idEl,
        width: width,
        height: height,
        left: 0,
        top: 0,
        head: {
            height: 35,
            type: 'line',
            cols: [
                {
                    cols: [
                        {width: 5},
                        {view: 'label', label: 'Текущее состояние', css: model.ids.header}
                    ]
                },
                {view: 'button', label: 'x', width: 30, click: model.hide}
            ]
        },
        body: {
            view: 'scrollview',
            scroll: 'y',
            body: {
                paddingX: 30,
                rows: [
                    {
                        cols: [
                            { width: 10},
                            {
                                view: 'template',
                                css: 'noBorder',
                                height: heightLabel,
                                template: ModalRealTime.labels.labelBalanceInUSD,
                                id: model.ids.labelBalanceInUSD
                            },
                            { width: 10},
                            {
                                view: 'template',
                                css: 'noBorder',
                                height: heightLabel,
                                template: ModalRealTime.labels.labelPositions,
                                id: model.ids.labelPositions
                            },
                            { width: 10},
                            /*{
                                view: 'template',
                                css: 'noBorder',
                                height: heightLabel,
                                template: ModalRealTime.labels.labelIncomeDrawdown,
                                id: model.ids.labelIncomeDrawdown
                            },
                            { width: 10},*/
                            {
                                view: 'template',
                                css: 'noBorder',
                                height: heightLabel,
                                template: ModalRealTime.labels.labelTimeWork,
                                id: model.ids.labelTimeWork
                            },
                            { width: 10},
                            {
                                view: 'template',
                                css: 'noBorder',
                                height: heightLabel,
                                template: ModalRealTime.labels.labelIncome,
                                id: model.ids.labelIncome
                            },
                            { width: 10},
                        ]
                    },
                    {height: 25},
                    {view: 'label', label: 'Операции'},
                    {height: 10},
                    {
                        view: 'datatable',
                        id: model.ids.tableOperations,
                        select: true,
                        scroll: true,
                        resizeColumn: true,
                        columns: [
                            {
                                id: 'index',
                                header: {text: '№', height: 35},
                                width: 45,
                                css: 'textAlignCenter'
                            },
                            {id: 'dataCreateOperation', width: 150, header: 'Дата и время', fillspace:true},
                            {id: 'countPositions', width: 100, header: 'Кол-во позиций', fillspace:true},
                            {id: 'operationType', width: 75, header: 'Операция', fillspace:true},
                            {id: 'price', width: 120, header: 'Цена позиции', fillspace:true},
                        ],
                    },
                    {height: 5},
                    {
                        cols: [
                            {view: 'button', label: 'График', click: model.openChartOperations, width: 150},
                            {}
                        ]
                    },
                    {height: 15},
                ]
            }
        }
    });
});

class ModalChartChangeBalance extends BaseView{
    init(){
        super.init();

        /**
         * @type {Chart}
         */
        this.chart = null;

        this.ids = {
            chartChangeBalance: this.idEl + '__chart-change-balance',
        };

        this.onResize = this.onResize.bind(this);
    }

    /**
     * @param {Array} data
     */
    show(data){
        super.show();

        // create the chart
        this.chart = Highcharts.stockChart(this.ids.chartChangeBalance, {
            series: [
                {
                    type: 'line',
                    data: data.map((el, i) => [i, el]),
                    name: 'Баланс',
                },
            ]
        });
    }

    onResize(){
        const container = $('#' + this.ids.chartChangeBalance);
        this.chart.chart.setSize(container.width(), container.height());
    }
}
ModalChartChangeBalance.instance = new ModalChartChangeBalance();

//View
webix.ready(function(){
    const model = ModalChartChangeBalance.instance;
    const body = $('body');
    const width = body.width() / 100 * 75;
    const height = body.height() / 100 * 75;

    webix.ui(WidgetModal.template(model, 'График изменения баланса', width, height, (body.width() - width) / 3, (body.height() - height) / 1.5, {
        padding: 5,
        rows: [
            { view: 'list', id: model.ids.chartChangeBalance },
        ]
    }, true, model.onResize));

    $($$(model.ids.chartChangeBalance).getNode()).attr('id', model.ids.chartChangeBalance);
});
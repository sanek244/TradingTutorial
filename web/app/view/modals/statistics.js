class ModalStatistics extends BaseView{
    init(){
        super.init();

        /**
         * @type {Array}
         */
        this.trades = [];

        this.ids = {
            labelDataChartBegin: this.idEl + '__date-chart-begin',
            labelDataChartCurrent: this.idEl + '__date-chart-current',
            labelDataChartPeriod: this.idEl + '__date-chart-period',
            labelDataBegin: this.idEl + '__date-begin',
            labelDataPeriod: this.idEl + '__date-period',
            labelIncome: this.idEl + '__income',
            labelAvrIncome: this.idEl + '__avr-income',
            labelAvrIncomeMedian: this.idEl + '__avr-income-median',
            labelAvrIncomeOnDay: this.idEl + '__avr-income-on-day',
            labelOldProgress: this.idEl + '__old-progress',
            labelAvrTimeLifePositions: this.idEl + '__avr-time-life-positions',
            labelBestTrade: this.idEl + '__best-trade',
            labelFailTrade: this.idEl + '__fail-trade',
            labelRatioTrades: this.idEl + '__ratio-trades',
            labelPercentProfitFromMax: this.idEl + '__percent-profit-from-max',
            labelCommissionSumAll: this.idEl + '__commission-sum-all',

            header: this.idEl + '__header',
            labelHeader: this.idEl + '__label-header',
            tableTrades: this.idEl + '__table-trades',

            buttonOpenTradeChart: this.idEl + '__button-open-trade-chart',

            buttonSave: this.idEl + '__button-save',
            buttonAllResults: this.idEl + '__button-all-results', 
        };

        /**
         * Всплывающие подсказки надписей
         * @type {{labelRatioTrades: string}}
         */
        this.labelValueTitles = {
            labelRatioTrades: 'удачные/нулевые/неудачные = всего трейдов',
        };

        /**
         *
         * @type {StatisticsData}
         */
        this.statisticsData = null;

        /**
         * Статистика по алгоритму?
         * @type {bool}
         */
        this.isAlgorithm = false;

        this.onResize = this.onResize.bind(this);
        this.openChartChangeBalance = this.openChartChangeBalance.bind(this);
        this.openChartTrades = this.openChartTrades.bind(this);
        this.saveCurrentResult = this.saveCurrentResult.bind(this);
        this.openAllResults = this.openAllResults.bind(this);
        $(window).resize(this.onResize);
    }


    /**
     * @param {StatisticsData} statisticsDataOther
     */
    show(statisticsDataOther){
        super.show();
        this.onResize();

        if(statisticsDataOther){
            this.isAlgorithm = true;
            $$(this.ids.buttonSave).hide();
            $$(this.ids.buttonAllResults).hide();
        }
        else{
            this.isAlgorithm = false;
            $$(this.ids.buttonSave).show();
            $$(this.ids.buttonAllResults).show();
        }

        this.statisticsData = statisticsDataOther || ViewMain.instance.game.statisticsData;
        if(!this.statisticsData){
            return;
        }

        this.setValueLabel(this.ids.labelDataChartBegin, this.statisticsData.dataChartBegin);
        this.setValueLabel(this.ids.labelDataChartCurrent, this.statisticsData.dataChartCurrent);
        this.setValueLabel(this.ids.labelDataChartPeriod, this.statisticsData.dataChartPeriod);
        this.setValueLabel(this.ids.labelIncome, this.statisticsData.endBalance, '', true);
        this.setValueLabel(this.ids.labelAvrIncome, this.statisticsData.avrIncome, '', true);
        this.setValueLabel(this.ids.labelAvrIncomeMedian, this.statisticsData.avrIncomeMedian, '', true);
        this.setValueLabel(this.ids.labelCommissionSumAll, this.statisticsData.commissionSumAll, '', true);
        this.setValueLabel(this.ids.labelAvrIncomeOnDay, this.statisticsData.avrIncomeOnDay, '', true);
        this.setValueLabel(this.ids.labelAvrTimeLifePositions, this.statisticsData.avrTimeLifePositions);
        this.setValueLabel(this.ids.labelBestTrade, this.statisticsData.bestTrade, '', true);
        this.setValueLabel(this.ids.labelFailTrade, this.statisticsData.failTrade, '', true);
        this.setValueLabel(this.ids.labelRatioTrades, this.statisticsData.ratioTrades);

        this.trades = this.statisticsData.trades.map(el => new Trade(el));
        $$(this.ids.tableTrades).clearAll();
        $$(this.ids.tableTrades).parse(this.trades);
        $$(this.ids.tableTrades).refresh();
    }

    setValueLabel(id, valueLabel, symbol = '', isColor = false){
        let cssValue = 'blackText';

        if(isColor){
            valueLabel = valueLabel + '';
            const number = valueLabel ? parseFloat(valueLabel.substring(0, valueLabel.length -1)) : 0;
            if(number > 0){
                cssValue = 'limeText boldText'
            }

            if(number < 0){
                cssValue = 'redText boldText';
            }
        }

        const idLabel = Object.keys(this.ids).filter(x => this.ids[x] === id)[0];

        $$(id).setHTML(
            '<span class="blueText">' +
                StatisticsData.labels[idLabel] +
            '</span>: ' +
            '<span class="' + cssValue + '" title="' + (idLabel in this.labelValueTitles ? this.labelValueTitles[idLabel] : '') + '">' +
            valueLabel + symbol +
            '</span>'
        );
    }

    /**
     * Открывает график изменения цены
     */
    openChartChangeBalance(){
        ModalChartChangeBalance.instance.show(this.statisticsData.changeBalance);
    }

    /**
     * Открывает график трейда
     */
    openChartTrade(idTrade){
        ModalChartTrade.instance.show(this.trades[this.trades.findIndex(x => x.id === idTrade)], this.isAlgorithm);
    }

    /**
     * Открывает график всех трейдов
     */
    openChartTrades(){
        ModalChartTrades.instance.show(this.trades, this.isAlgorithm);
    }

    onResize(){
        if(this.el && this.el.isVisible()){
            const body = $('body');
            const width = body.width();
            const height = body.height();
            this.el.$setSize(width, height);
        }
    }

    key(){
        //keys
        $(document).on('keydown', (event) => {
            if(this.el && this.el.isVisible() && !ModalChartTrade.instance.el.isVisible()) {
                switch (event.keyCode) {
                    case 27: this.hide(); break; //ESC
                }

                if(event.keyCode === 27){
                    event.preventDefault();
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                }
            }
        });
    }

    /**
     * Сохранение текущих достижений
     */
    saveCurrentResult(){
        GameResults.save(this.statisticsData);

        //TODO: save to Google drive

        webix.message({
            text: "Данные успешно сохранены!",
            type:"success",
            expire: 5000,
            id:"save_data"
        });

        $$(this.ids.buttonSave).hide();
    }

    openAllResults(){
        this.hide();
        ModalSavedResults.instance.show();
    }
}
ModalStatistics.instance = new ModalStatistics();

webix.ready(() => {
    const model = ModalStatistics.instance;
    const body = $('body');
    const width = body.width();
    const height = body.height();
    const heightLabel = 30;

    //технические переменные
    let onAfterRenderSettings = false;

    webix.ui({
        view: 'window',
        modal: true,
        id: model.idEl,
        width: width,
        height: height,
        left: 0,
        top: 0,
        head: {
            height: 35,
            type: 'line',
            cols: [
                {
                    cols: [
                        {width: 5},
                        {view: 'label', label: 'Статистика', css: model.ids.header}
                    ]
                },
                {view: 'button', label: 'x', width: 30, click: model.hide}
            ]
        },
        body: {
            view: 'scrollview',
            scroll: 'y',
            body: {
                paddingX: 30,
                rows: [
                    {
                        cols: [
                            {
                                rows: [
                                    {height: 20},
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelDataChartBegin,
                                        id: model.ids.labelDataChartBegin
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelDataChartCurrent,
                                        id: model.ids.labelDataChartCurrent
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelDataChartPeriod,
                                        id: model.ids.labelDataChartPeriod
                                    },
                                    {height: 20},
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelIncome,
                                        id: model.ids.labelIncome
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelAvrIncome,
                                        id: model.ids.labelAvrIncome
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelAvrIncomeMedian,
                                        id: model.ids.labelAvrIncomeMedian
                                    },
                                    {
                                        cols: [
                                            {
                                                view: 'template',
                                                css: 'noBorder',
                                                height: heightLabel,
                                                template: StatisticsData.labels.labelAvrIncomeOnDay,
                                                id: model.ids.labelAvrIncomeOnDay,
                                                width: 300
                                            },
                                            {width: 100},
                                            /*{
                                                view: 'template',
                                                css: 'noBorder',
                                                height: heightLabel,
                                                template: StatisticsData.labels.labelOldProgress,
                                                id: model.ids.labelOldProgress
                                            },
                                            {}*/
                                        ]
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelCommissionSumAll,
                                        id: model.ids.labelCommissionSumAll
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelAvrTimeLifePositions,
                                        id: model.ids.labelAvrTimeLifePositions
                                    },
                                    {height: 20},
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelBestTrade,
                                        id: model.ids.labelBestTrade
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelFailTrade,
                                        id: model.ids.labelFailTrade
                                    },
                                    {
                                        view: 'template',
                                        css: 'noBorder',
                                        height: heightLabel,
                                        template: StatisticsData.labels.labelRatioTrades,
                                        id: model.ids.labelRatioTrades
                                    },
                                    {}
                                ]
                            },
                            {
                                rows: [
                                    {view: 'label', label: 'Трейды', css: model.ids.labelHeader},
                                    {height: 10},
                                    {
                                        view: 'datatable',
                                        id: model.ids.tableTrades,
                                        select: true,
                                        scroll: true,
                                        resizeColumn: true,
                                        columns: [
                                            {
                                                id: 'index',
                                                header: {text: '№', height: 35},
                                                width: 45,
                                                css: 'textAlignCenter'
                                            },
                                            {id: 'income', width: 150, header: 'Прибыль', template: '#income#$ (#incomePercent#%)', sort: 'int'},
                                            {id: 'maxCountPositions', width: 100, header: 'Позиций', sort: 'int'},
                                            {id: 'countOperations', width: 75, header: 'Операций', sort: 'int'},
                                            {id: 'timeLife', width: 120, header: 'Время'},
                                            {id: 'drawdownPercent', width: 120, header: 'Просадка %', template: '#drawdownPercent#%', sort: 'int'},
                                            {id: 'startDate', width: 160, header: 'Дата начала', sort: 'dateTime'},
                                            {
                                                id: 'button-open-chart',
                                                width: 150,
                                                header: 'График',
                                                template: function (obj) {
                                                    return '<div class="webix_el_button">' +
                                                            ' <button ' +
                                                                'class="webixtype_base ' + model.ids.buttonOpenTradeChart + '"' +
                                                                'onclick="ModalStatistics.instance.openChartTrade(' + obj.id + ')"' +
                                                            '>' +
                                                                'Открыть' +
                                                            '</button>' +
                                                        '</div>';
                                                }
                                            },
                                        ],
                                        on: {
                                            'onColumnResize': (idColumn, newWidth, oldWidth, user_action) => {
                                                //save
                                                if (user_action) {
                                                    let localeData = localStorage.getItem(model.ids.tableTrades);
                                                    if (localeData === 'undefined') {
                                                        localeData = '{}';
                                                    }

                                                    const table = JSON.parse(localeData) || {};
                                                    if (!table[idColumn]) {
                                                        table[idColumn] = {};
                                                    }
                                                    table[idColumn].width = newWidth;
                                                    localStorage.setItem(model.ids.tableTrades, JSON.stringify(table));
                                                }
                                            },
                                            'data->onStoreUpdated': function () {
                                                //index
                                                this.data.each(function (obj, i) {
                                                    obj.index = i + 1;
                                                })
                                            },
                                            'onAfterRender': function () {
                                                const idTable = model.ids.tableTrades;

                                                if (localStorage.getItem(idTable) && !onAfterRenderSettings) {
                                                    onAfterRenderSettings = true;

                                                    //настройка столбцов
                                                    let localeData = localStorage.getItem(idTable);
                                                    if (localeData === 'undefined') {
                                                        localeData = '{}';
                                                    }

                                                    const table = JSON.parse(localeData) || {};
                                                    Object.keys(table).map(key => {
                                                        if (table[key].width && $$(idTable).isColumnVisible(key)) {
                                                            $$(idTable).setColumnWidth(key, table[key].width);
                                                        }
                                                        if (table[key].hidden) {
                                                            $$(idTable).hideColumn(key);
                                                        }
                                                        else {
                                                            $$(idTable).showColumn(key);
                                                        }
                                                        if (table[key].index) {
                                                            $$(idTable).moveColumn(key, table[key].index);
                                                        }
                                                    });
                                                }

                                                this.data.each((obj, i) => {
                                                    let css = '';
                                                    if (obj.income > 0) {
                                                        css = 'greenBack whiteText';
                                                    }
                                                    else if (obj.income < 0) {
                                                        css = 'redBack whiteText';
                                                    }
                                                    obj.incomeShow = ViewMain.instance.roundToPrice(obj.income) + ViewMain.instance.symbol;
                                                    if (css) {
                                                        $(this.getNode()).find('[aria-rowindex="' + (i + 1) + '"]').addClass(css);
                                                    }
                                                });
                                            },
                                        },
                                    },
                                    {height: 5},
                                    {
                                        cols: [
                                            {view: 'button', label: 'Одним графиком', click: model.openChartTrades, width: 150},
                                            {}
                                        ]
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        cols: [
                            {view: 'button', label: 'Назад', click: model.hide, width: 150},
                            {view: 'button', label: 'Изменения баланса', click: model.openChartChangeBalance, width: 200},
                            {view: 'button', label: 'Сохранить результат', click: model.saveCurrentResult, width: 200, id: model.ids.buttonSave},
                            {view: 'button', label: 'Все результаты', click: model.openAllResults, width: 200, id: model.ids.buttonAllResults},
                            {}
                        ]
                    },
                    {height: 20}
                ]
            }
        }
    });
});

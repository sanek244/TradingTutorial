class ModalAlert extends BaseView {
    init(){
        super.init();

        this.textAll = '';
        this.ids = {
            message: this.idEl + '__message',
            buttonOpenAllText: this.idEl + '__button-open-all-text',
        };

        this.width = 450;
        this.height = 200;

        this.openAllText = this.openAllText.bind(this);
    }

    show(textCommon, textAll){
        super.show();

        this.el.$setSize(this.width, this.height);
        $$(this.ids.message).setHTML(textCommon);
        if(!textAll){
            $$(this.ids.buttonOpenAllText).hide();
        }
        else{
            this.el.$setSize(this.width, this.height + 10);
            $$(this.ids.buttonOpenAllText).show();
        }
        this.textAll = textAll;
    }

    openAllText(){
        $$(this.ids.message).setHTML(this.textAll);
        $$(this.ids.buttonOpenAllText).hide();
        if(this.textAll.length > 35){
            const newWidth = 350 + this.textAll.length * 2;
            const newHeight = 200 + this.textAll.length / 4;
            const body = $('body');
            this.el.$setSize(
                newWidth > body.width() ? body.width() - 100 : newWidth,
                newHeight > body.height() ? body.height() - 100 : newHeight);
        }
    }

    key(){
        $(document).on('keydown', (event) => {
            if(this.el && this.el.isVisible()){

                switch(event.keyCode){
                    case 13:   //Enter
                    case 27:  this.hide(); break; //Esc
                }

                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }
        });
    }
}
ModalAlert.instance = new ModalAlert();


//View
webix.ready(function(){
    const model = ModalAlert.instance;

    webix.ui(WidgetModalFixed.template(model.idEl, 'ПРЕДУПРЕЖДЕНИЕ', model.width, model.height, 350, 200, 0, 'info', {
        padding: 5,
        rows: [
            { view: 'template', id: model.ids.message, css: 'modalBlackText modalInfo noBorder', template: ''},
            { height: 15 },
            {
                view: 'button',
                id: model.ids.buttonOpenAllText,
                value: 'Сообщение полностью ▼',
                css: model.ids.buttonOpenAllText,
                click: model.openAllText
            },
            {
                cols: [
                    {  },
                    {
                        view: 'button',
                        value: 'Ок',
                        css: 'blueButton',
                        width: 150,
                        height: 45,
                        click: model.hide
                    },
                    {  },
                ]
            },
            {height: 5}
        ]
    }, null, true));
});
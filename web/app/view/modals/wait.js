class ModalWait extends BaseView{
    init(){
        super.init();

        this.ids = {
            message: this.idEl + '__message',
            img: this.idEl + '__img',
            imgContainer: this.idEl + '__img-container',
        };
    }

    show(text){
        super.show();

        $($$(this.ids.message).$view).find('div').text(text);
    }

    key(){
        //keys
        $(document).on('keydown', (event) => {
            if(this.el && this.el.isVisible()){
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }
        });
    }
}
ModalWait.instance = new ModalWait();


webix.ready(function(){
    const model = ModalWait.instance;

    webix.ui({
        view: 'window',
        modal: true,
        id: model.idEl,
        width: 350,
        height: 150,
        left: 450,
        top: 250,
        head: false,
        body: {
            css: 'wait',
            rows: [
                { view: 'template', id: model.ids.message, css: model.ids.message, template: '', height: 55 },
                { view: 'template', template: '<img src="media/wait.png" class="' + model.ids.img + '"/>', css: model.ids.imgContainer },
            ]
        },
    });
});
class ModalChartBestGame extends BaseView{
    init(){
        super.init();

        /**
         * @type {Chart}
         */
        this.chart = null;

        this.ids = {
            chartChanges: this.idEl + '__chart-changes',
            resultLabel: this.idEl + '__chart-result',
        };

        this.onResize = this.onResize.bind(this);
    }

    show(){
        super.show();

        if(this.chart === null){
            this.chart = new Chart(this.ids.chartChanges, [], ViewMain.instance.game.chart.name);
            this.chart.seriesOn.MACD = false;
            this.chart.seriesOn.volume = false;
            this.chart.seriesOn.slidingLine50 = false;
            this.chart.seriesOn.slidingLine100 = true;
            this.chart.seriesOn.slidingLine200 = false;
            this.chart.seriesOn.operationIndicators = true;
        }

        this.chart.data = this.calculateBestGame(ViewMain.instance.game.data.slice(0, ViewMain.instance.game.endBarIndex));
    }

    calculateBestGame(data){
        let index = 7;
        let shift = 35;
        let isUp = data[0][index] < data[1][index];
        let isUpIndex = isUp;
        let lastChangeIndex = 0;
        let lastOperationIndex = 0;
        let balance = ViewMain.instance.game.startMoney;
        let amount = 0;
        let lastSuperPrice = data[0][1];
        console.log('balance', {balance});
        for(let i = 50; i < data.length - 1; i++){
            if(isUpIndex && data[i][index] < data[i + 1][index] && i - lastChangeIndex > 1){
                lastChangeIndex = i;
                isUpIndex = false;
            }

            if(!lastOperationIndex || isUp && lastChangeIndex != 0 && data[lastChangeIndex][index] < data[i][index] * 0.90){
                let element = data.slice((lastOperationIndex == 0 ? 0 : Math.max(lastChangeIndex - shift * 2, 0)), lastChangeIndex + shift * 1).filter(x => x[0] > lastOperationIndex).min(x => x[1]);
                element.push([
                    this.getPercentageChange(lastSuperPrice, element[1]),
                    TypeOperation.LONG
                ]);
                lastSuperPrice = element[1];
                amount = balance / element[1];
                balance = 0; 
                isUpIndex = false;
                isUp = false;
                lastChangeIndex = 0;
                lastOperationIndex = element[0];
                console.log('buy', {time: element[0], time2: new Date(element[0]), amount, price: lastSuperPrice});
            }

            if(!isUpIndex && data[i][index] > data[i + 1][index] && i - lastChangeIndex > 1){
                lastChangeIndex = i;
                isUpIndex = true;
            }

            if(!isUp && lastChangeIndex != 0 && data[lastChangeIndex][index] > data[i][index] * 1.1){
                let element = data.slice(Math.max(lastChangeIndex - shift * 2, 0), lastChangeIndex + shift * 1).filter(x => x[0] > lastOperationIndex).max(x => x[1]);
                if(amount){
                    element.push([
                        this.getPercentageChange(lastSuperPrice, element[1]),
                        TypeOperation.SELL
                    ]);
                }
                lastSuperPrice = element[1];
                if(amount){
                    console.log('sell', {time: element[0], time2: new Date(element[0]), amount, price: lastSuperPrice, newBalance: amount * element[1]});
                    balance = amount * element[1];
                    amount = 0;
                    lastOperationIndex = element[0];
                }
                isUpIndex = true;
                isUp = true;
                lastChangeIndex = 0;
            }
        }

        if(amount > 0){
            let element = data.slice(Math.max(data.length - shift * 2, 0)).filter(x => x[0] > lastOperationIndex).max(x => x[1]);
            element.push([
                this.getPercentageChange(lastSuperPrice, element[1]),
                TypeOperation.SELL
            ]);
            balance = amount * element[1];
            console.log('sell - finish', {amount, element, price: element[1], newBalance: balance});
        }


        $$(this.ids.resultLabel).setHTML('Результат "Идеальных" трейдов: <b>$' + Helper.formatPrice(Math.floor(balance)) + '</b>');
        console.log('Динамика изменений', data.map(x => x[9]).filter(x => x).map(x => x[0]));

        return data;
    }

    onResize(){
        const container = $('#' + this.ids.chartChanges);
        this.chart.chart.setSize(container.width(), container.height());
    }

    getPercentageChange(lastPrice, newPrice){
        if(newPrice > lastPrice){
            return '+' + Math.round(newPrice/ lastPrice * 100 - 100) + '%';
        }

        return '-' + Math.round(100 - newPrice / lastPrice * 100) + '%';
    }
}
ModalChartBestGame.instance = new ModalChartBestGame();

//View
webix.ready(function(){
    const model = ModalChartBestGame.instance;
    const body = $('body');
    const width = body.width() / 100 * 75;
    const height = body.height() / 100 * 75;

    webix.ui(WidgetModal.template(model, 'График "Идеальных" трейдов', width, height, (body.width() - width) / 3, (body.height() - height) / 1.5, {
        padding: 5,
        rows: [
            { view: 'list', id: model.ids.chartChanges },
            { view: 'label', label: 'Результат "Идеальных" трейдов: ', id: model.ids.resultLabel},
        ]
    }, true, model.onResize));

    $($$(model.ids.chartChanges).getNode()).attr('id', model.ids.chartChanges);
});
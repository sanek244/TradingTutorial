class ModalChartTrade extends BaseView{
    init(){
        super.init();

        /**
         * @type {Chart}
         */
        this.chart = null;

        this.ids = {
            chartTrade: this.idEl + '__chart-trade',
            chartTrade2: this.idEl + '__chart-trade2',
        };

        this.onResize = this.onResize.bind(this);
    }

    /**
     * @param {Trade} trade
     */
    show(trade){
        super.show();

        if(this.chart === null){
            this.chart = new Chart(this.ids.chartTrade, [], ViewMain.instance.game.chart.name);
            this.chart.seriesOn.operationIndicators = true;
            this.chart.seriesOn.MACD = true;
            this.chart.seriesOn.volume = true;
            this.chart.seriesOn.slidingLine50 = true;
            this.chart.seriesOn.slidingLine100 = true;
            this.chart.seriesOn.slidingLine200 = true;
        }

        const shift = 15;
        let startShift = trade.operations[0].indexChartBar > shift ? shift : trade.operations[0].indexChartBar;
        let startBar = trade.operations[0].indexChartBar - shift - 1;
        //trade.operations[0].indexChartBar = 0, shift = 15, startBar = -16, startShift = 0;
        //trade.operations[0].indexChartBar = 5, shift = 15, startBar = -11, startShift = 5
        if(startBar < 0){
            startBar = 0;
        }

        let endBar = trade.operations.last().indexChartBar + shift;
        if(endBar > ViewMain.instance.game.endBarIndex){
            endBar = ViewMain.instance.game.endBarIndex;
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const data = ViewMain.instance.game.data.slice(startBar, endBar).cloneDeep();
        trade.operations.map(operation => {
            const point = [
                operation.countPositions,
                operation.operationType
            ];
            data[operation.indexChartBar - trade.operations[0].indexChartBar + startShift + 1].push(point);
        });
        this.chart.data = data;
    }

    /**
     * @param {Trade} trade
     */
    showAlgorithmTrade(trade){

        const bases = [];
        ViewMain.instance.game.dataFull.map(el => {
            if(bases.indexOf(el['base']) === -1){
                bases.push(el['base']);
            }
        });
        if(bases.filter(x => x).length > 1){
            ViewMain.instance.algorithm = ViewMain.instance.game;
            return this.showOperations2TF(trade.operations);
        }




        if(this.chart === null){
            this.chart = new ChartCustom(this.ids.chartTrade, [], ViewMain.instance.game.chart.name);
        }

        const shift = 15;
        let startBar = trade.operations[0].indexChartBar - shift - 1;
        if(startBar < 0){
            startBar = 0;
        }

        let endBar = trade.operations.last().indexChartBar + shift;
        if(endBar > ViewMain.instance.game.endBarIndex){
            endBar = ViewMain.instance.game.endBarIndex;
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const dateTimeStart = ViewMain.instance.game.data[startBar][0];
        const dateTimeEnd = ViewMain.instance.game.data[endBar][0];

        const data = ViewMain.instance.game.dataFull.map(dataEl => {
            if(dataEl['method'] === 'MACD'){
                const start = dataEl['data'][0].findIndex(x => x[0] > dateTimeStart);
                const end = dataEl['data'][0].findIndex(x => x[0] > dateTimeEnd);
                const newEl = JSON.parse(JSON.stringify(dataEl));
                newEl['data'][0] = newEl['data'][0].slice(start - 1, end);
                newEl['data'][1] = newEl['data'][1].slice(start - 1, end);
                newEl['data'][2] = newEl['data'][2].slice(start - 1, end);
                return newEl;
            }
            else{
                const start = dataEl['data'].findIndex(x => x[0] > dateTimeStart);
                const end = dataEl['data'].findIndex(x => x[0] > dateTimeEnd);
                const newEl = Object.assign({}, dataEl);
                newEl['data'] = dataEl['data'].slice(start - 1, end);
                return newEl;
            }
        });
        data.push({data: [], method: 'operations'});

        trade.operations.map(operation => {
            data.last()['data']
                .push({
                    x: new Date(operation.dateChart).getTime(),
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });
        this.chart.data = data;
    }

    showOperations2TF(operations){

        if(this.chart === null){
            this.chart = new ChartCustom(this.ids.chartTrade, [], '1m');
            this.chart2 = new ChartCustom(this.ids.chartTrade2, [], '5m');
            $$(this.ids.chartTrade2).show();
        }

        const shift = 15;
        let startBar = operations[0].indexChartBar - shift - 1;
        if(startBar < 0){
            startBar = 0;
        }

        let endBar = operations.last().indexChartBar + shift;
        if(endBar > ViewMain.instance.game.endBarIndex){
            endBar = ViewMain.instance.game.endBarIndex;
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const dateTimeStart = ViewMain.instance.game.data[startBar][0];
        const dateTimeEnd = ViewMain.instance.game.data[endBar][0];
        //данные графика во время трейда + маркеры операций (sell/buy)
        let data = JSON.parse(JSON.stringify(ViewMain.instance.algorithm.dataFull));
        data = data
            .filter(x => {
                if('base' in x){
                    return x['base'] === data[0]['name'];
                }
                return true;
            })
            .map(dataEl => {
                if(dataEl['method'] === 'MACD'){
                    const start = dataEl['data'][0].findIndex(x => x[0] > dateTimeStart);
                    const end = dataEl['data'][0].findIndex(x => x[0] > dateTimeEnd);
                    const newEl = JSON.parse(JSON.stringify(dataEl));
                    newEl['data'][0] = newEl['data'][0].slice(start - 1, end);
                    newEl['data'][1] = newEl['data'][1].slice(start - 1, end);
                    newEl['data'][2] = newEl['data'][2].slice(start - 1, end);
                    return newEl;
                }
                else{
                    const start = dataEl['data'].findIndex(x => x[0] > dateTimeStart);
                    const end = dataEl['data'].findIndex(x => x[0] > dateTimeEnd);
                    const newEl = Object.assign({}, dataEl);
                    newEl['data'] = dataEl['data'].slice(start - 1, end);
                    return newEl;
                }
            });
        data.push({data: [], method: 'operations'});

        operations.map(operation => {
            data.last()['data']
                .push({
                    x: operation.dateChart,
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });
        this.chart.data = data;



        let data2 = JSON.parse(JSON.stringify(ViewMain.instance.algorithm.dataFull));
        delete data2[0];
        data2 = data2
            .filter(x => {
                if('base' in x){
                    return x['base'] === data2[1]['name'];
                }
                else{
                    return true;
                }

                return false;
            })
            .map(dataEl => {
                if(dataEl['method'] === 'MACD'){
                    const start = dataEl['data'][0].findIndex(x => x[0] > dateTimeStart);
                    const end = dataEl['data'][0].findIndex(x => x[0] > dateTimeEnd);
                    const newEl = JSON.parse(JSON.stringify(dataEl));
                    newEl['data'][0] = newEl['data'][0].slice(start - 1, end);
                    newEl['data'][1] = newEl['data'][1].slice(start - 1, end);
                    newEl['data'][2] = newEl['data'][2].slice(start - 1, end);
                    return newEl;
                }
                else{
                    const start = dataEl['data'].findIndex(x => x[0] > dateTimeStart);
                    const end = dataEl['data'].findIndex(x => x[0] > dateTimeEnd);
                    const newEl = Object.assign({}, dataEl);
                    newEl['data'] = dataEl['data'].slice(start - 1, end);
                    return newEl;
                }
            });

        data2.push({data: [], method: 'operations'});
        operations.map(operation => {
            data2.last()['data']
                .push({
                    x: operation.dateChart,
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });

        this.chart2.data = data2;
    }

    onResize(){
        const container = $('#' + this.ids.chartTrade);
        this.chart.chart.setSize(container.width(), container.height());
        this.chart2.chart.setSize(container.width(), container.height());
    }
}
ModalChartTrade.instance = new ModalChartTrade();

//View
webix.ready(function(){
    const model = ModalChartTrade.instance;
    const body = $('body');
    const width = body.width() / 100 * 75;
    const height = body.height() / 100 * 75;

    webix.ui(WidgetModal.template(model, 'График трейда', width, height, (body.width() - width) / 3, (body.height() - height) / 1.5, {
        padding: 5,
        cols: [
            { view: 'list', id: model.ids.chartTrade },
            { view: 'list', id: model.ids.chartTrade2, hidden: true },
        ]
    }, true, model.onResize));

    $($$(model.ids.chartTrade).getNode()).attr('id', model.ids.chartTrade);
    $($$(model.ids.chartTrade2).getNode()).attr('id', model.ids.chartTrade2);
});
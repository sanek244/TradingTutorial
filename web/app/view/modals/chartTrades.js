class ModalChartTrades extends BaseView{
    init(){
        super.init();

        /**
         * @type {Chart}
         */
        this.chart = null;

        this.ids = {
            chartTrade: this.idEl + '__chart-trades',
            chartTrade2: this.idEl + '__chart-trades2',
        };

        this.onResize = this.onResize.bind(this);
    }

    /**
     * @param {Trade[]} trades
     * @param {bool} isAlgorithm - Результат алгоритма?
     */
    show(trades, isAlgorithm){
        super.show();

        if(isAlgorithm){
            return this.showAlgorithmTrade(trades);
        }

        if(this.chart === null){
            this.chart = new Chart(this.ids.chartTrade, [], ViewMain.instance.game.chart.name);
            this.chart.seriesOn.operationIndicators = true;
            this.chart.seriesOn.MACD = true;
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const data = ViewMain.instance.game.data.cloneDeep().slice(0, ViewMain.instance.game.endBarIndex);
        trades.map(trade => {
            trade.operations.map(operation => {
                const point = [
                    operation.countPositions,
                    operation.operationType
                ];
                data[operation.indexChartBar].push(point);
            });
        });
        this.chart.data = data;
    }

    /**
     * @param {Trade[]} trades
     */
    showAlgorithmTrade(trades){

        if(this.chart === null){
            this.chart = new ChartCustom(this.ids.chartTrade, [], ViewMain.instance.game.chart.name);
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const data = JSON.parse(JSON.stringify(ViewMain.instance.game.dataFull));
        data.push({data: [], method: 'operations'});
        trades.map(trade => {
            trade.operations.map(operation => {
                data.last()['data']
                    .push({
                        x: new Date(operation.dateChart).getTime(),
                        text: operation.countPositions,
                        title: operation.operationType
                    });
            });
        });
        this.chart.data = data;
    }

    showOperations(operations){
        super.show();

        const bases = [];
        ViewMain.instance.algorithm.dataFull.map(el => bases.push(el['base']));
        console.log(bases.filter(x => x), ViewMain.instance.algorithm.dataFull);
        if(bases.filter(x => x).length > 1){
            return this.showOperations2TF(operations);
        }

        if(this.chart === null){
            this.chart = new ChartCustom(this.ids.chartTrade, [], ViewMain.instance.game.chart.name);
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        const data = JSON.parse(JSON.stringify(ViewMain.instance.game.dataFull));
        data.push({data: [], method: 'operations'});
        operations.map(operation => {
            data.last()['data']
                .push({
                    x: new Date(operation.dateChart).getTime(),
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });
        this.chart.data = data;
    }


    showOperations2TF(operations){
        console.log('showOperations2TF');
        super.show();


        if(this.chart === null){
            this.chart = new ChartCustom(this.ids.chartTrade, [], '1m');
            this.chart2 = new ChartCustom(this.ids.chartTrade2, [], '5m');
        }

        //данные графика во время трейда + маркеры операций (sell/buy)
        let data = JSON.parse(JSON.stringify(ViewMain.instance.algorithm.dataFull));
        data = data.filter(x => {
            if('base' in x){
                return x['base'] === data[0]['name'];
            }
            else{
                return true;
            }

            return false;
        });
        data.push({data: [], method: 'operations'});
        operations.map(operation => {
            data.last()['data']
                .push({
                    x: operation.dateChart,
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });
        this.chart.data = data;

        let data2 = JSON.parse(JSON.stringify(ViewMain.instance.algorithm.dataFull));
        delete data2[0];
        data2 = data2.filter(x => {
            if('base' in x){
                return x['base'] === data2[1]['name'];
            }
            else{
                return true;
            }

            return false;
        });

        data2.push({data: [], method: 'operations'});
        operations.map(operation => {
            data2.last()['data']
                .push({
                    x: operation.dateChart,
                    text: operation.countPositions,
                    title: operation.operationType
                });
        });

        this.chart2.data = data2;
    }

    onResize(){
        const container = $('#' + this.ids.chartTrade);
        this.chart.chart.setSize(container.width(), container.height());
    }
}
ModalChartTrades.instance = new ModalChartTrades();

//View
webix.ready(function(){
    const model = ModalChartTrades.instance;
    const body = $('body');
    const width = body.width() / 100 * 75;
    const height = body.height() / 100 * 75;

    webix.ui(WidgetModal.template(model, 'График операций за последнее время', width, height, (body.width() - width) / 3, (body.height() - height) / 1.5, {
        padding: 5,
        cols: [
            { view: 'list', id: model.ids.chartTrade },
            { view: 'list', id: model.ids.chartTrade2, hidden: true },
        ]
    }, true, model.onResize));

    $($$(model.ids.chartTrade).getNode()).attr('id', model.ids.chartTrade);
});
class ModalSavedResults extends BaseView{
    init(){
        super.init();

        this.ids = {
            tableAllResults: this.idEl + '__table-all-results',
            buttonDeleteResult: this.idEl + '__button-delete-result',
            buttonInfo: this.idEl + '__button-info',
        };
    }

    show(){
        super.show();
    
        this.results = GameResults.getResults();
        this.results.map(x => x['id'] = 'id' in x ? x['id'] : Helper.generateUid());
    
        $$(this.ids.tableAllResults).clearAll();
        $$(this.ids.tableAllResults).parse(this.results.reverse());
        $$(this.ids.tableAllResults).refresh();
    }
    
    /**
     * Удаляет запись
     * @param id
     */
    deleteResult(id){
        if(confirm('Вы действительно хотите удалить элемент?')){
            this.results.splice(this.results.findIndex(x => x.id === id), 1);
            let x = new GameResults();
            x.results = Helper
                .clone(this.results)
                .reverse()
                .map(el => {
                    delete el.id;
                    delete el.index;
                    return el;
                });
            x.save();
    
            $$(this.ids.tableAllResults).clearAll();
            $$(this.ids.tableAllResults).parse(this.results);
            $$(this.ids.tableAllResults).refresh();
        }
    }
    
    /**
     * Просмотреть подробно
     * @param id
     */
    viewResult(id){
        ModalStatistics.instance.show(this.results.find(x => x.id === id));
    }
}
ModalSavedResults.instance = new ModalSavedResults();

//View
webix.ready(function(){
    const model = ModalSavedResults.instance;
    const body = $('body');
    const width = body.width();
    const height = body.height();

    webix.ui(WidgetModal.template(model, 'Все сохранённые результаты', width, height, 1, 1, {
        view: 'datatable',
        id: model.ids.tableAllResults,
        scroll: 'y',
        select: true,
        columns: [
            { id:'index',           header: {text: '№', height: 35},   width: 45   },
            { id:'dateEnd', width: 150, header: 'Дата Сохранения', template: obj => Helper.getDate(obj.dateEnd), sort: 'dateTime'},
            { id:'stockName', width: 100, header: 'Акция', sort: 'string'},
            { id:'endBalance', width: 100, header: 'Прибыль', sort: 'number'},
            { id:'dataChartPeriod', width: 200, header: 'Продолжительность графика'},
            { width: 75, header: 'Удалить', template: obj => {
                return '<div class="webix_el_button" style="width: 40px; margin: auto;">' +
                    '<button ' +
                    '   class="webixtype_danger ' + model.ids.buttonDeleteResult + '"' +
                    '   onclick="ModalSavedResults.instance.deleteResult(\'' + obj.id + '\')"' +
                    '>' +
                        '<span class="webix_icon fas fa-trash"></span>' +
                    '</button>' +
                    '</div>';
            }},
            { width: 90, header: 'Подробно', template: obj => {
                return '<div class="webix_el_button" style="width: 40px; margin: auto;">' +
                    '<button ' +
                    '   class="webixtype_base ' + model.ids.buttonInfo + '"' +
                    '   onclick="ModalSavedResults.instance.viewResult(\'' + obj.id + '\')"' +
                    '>' +
                        '<span class="webix_icon fa-info "></span>' +
                    '</button>' +
                    '</div>';
            }},

        ],
        on: {
            'data->onStoreUpdated': function () {
                this.data.each((obj, i) => obj.index = i + 1)
            }
        },
        data: [],
    }));
});
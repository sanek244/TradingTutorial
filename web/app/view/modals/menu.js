class ModalMenu extends BaseView{
    init(){
        super.init();

        this.ids = {
            buttonContinue: this.idEl + '__button-continue',
            buttonStatistics: this.idEl + '__button-statistics',
            buttonChartBestGame: this.idEl + '__button-chart-changes',
            //buttonCAllResults: this.idEl + '__button-all-results',
            space1: this.idEl + '__space-1',
            space2: this.idEl + '__space-2',
            space3: this.idEl + '__space-3',
        };

        this.heightMenu = 275;
        this.heightMenu2 = 180;
        this.widthMenu = 300;
        this.onClickNewGame = this.onClickNewGame.bind(this);
        this.onClickStatistic = this.onClickStatistic.bind(this);
        this.onClickChartBestGame = this.onClickChartBestGame.bind(this);
        this.onClickAllResults = this.onClickAllResults.bind(this);
        
    }

    show(){
        super.show();

        if(ViewMain.instance.game === null){
            $$(this.ids.buttonContinue).hide();
            $$(this.ids.buttonStatistics).hide();
            $$(this.ids.buttonChartBestGame).hide();
            $$(this.ids.space1).hide();
            $$(this.ids.space2).hide();
            $$(this.ids.space3).hide();
            this.el.$setSize(this.widthMenu, this.heightMenu2);
        }
        else{
            $$(this.ids.buttonContinue).show();
            $$(this.ids.buttonStatistics).show();
            $$(this.ids.buttonChartBestGame).show();
            $$(this.ids.space1).show();
            $$(this.ids.space2).show();
            $$(this.ids.space3).show();
            this.el.$setSize(this.widthMenu, this.heightMenu);
        }
    }

    onClickNewGame(){
        this.hide();
        ModalSelectStock.instance.show();
    }

    onClickStatistic(){
        this.hide();
        ModalStatistics.instance.show();
    }

    onClickChartBestGame(){
        this.hide();
        ModalChartBestGame.instance.show();
    }

    onClickAllResults(){
        this.hide();
        ModalSavedResults.instance.show();
    }
}
ModalMenu.instance = new ModalMenu();


webix.ready(() => {
    const model = ModalMenu.instance;

    webix.ui(WidgetModal.template(model, 'Меню', model.widthMenu, model.heightMenu, 550, 200, {
        padding: 30,
        rows: [
            {view: 'button', label: 'Новая партия', click: model.onClickNewGame},
            {id: model.ids.space1, height: 10},
            {view: 'button', label: 'Продолжить', id: model.ids.buttonContinue, click: model.hide},
            {id: model.ids.space2, height: 10},
            {view: 'button', label: 'Статистика', id: model.ids.buttonStatistics, click: model.onClickStatistic},
            {id: model.ids.space3, height: 10},
            {view: 'button', label: 'Имитация идеальной игры', id: model.ids.buttonChartBestGame, click: model.onClickChartBestGame},
            {id: model.ids.space4, height: 10},
            //{view: 'button', label: 'Все предыдущие результаты', id: model.ids.buttonCAllResults, click: model.onClickAllResults},
            //{id: model.ids.space5, height: 10},
        ]
    }));
});

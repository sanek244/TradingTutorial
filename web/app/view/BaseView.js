class BaseView extends BaseModel{
    init(){

        /**
         * id формы
         * @type {string}
         */
        this.idEl = this.className();

        //включение контроля формой через клавишы
        this.key();

        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
    }

    /**
     * webix объект формы
     * @type {object}
     */
    get el() {
        return $$(this.idEl);
    }

    /**
     * Показать форму
     */
    show(){
        this.el.show();
        ViewMain.instance.isVisibleModals[this.idEl] = true;
    }

    /**
     * скрыть форму
     */
    hide(){
        this.el.hide();
        ViewMain.instance.isVisibleModals[this.idEl] = false;
    }

    /**
     * Контроль формы через нажатия клавиш
     */
    key(){
        //keys
        $(document).on('keydown', (event) => {
            if(this.el && this.el.isVisible() && !ModalAlert.instance.el.isVisible() && !ModalEndAlert.instance.el.isVisible() && !ModalWait.instance.el.isVisible()){

                switch(event.keyCode){
                    case 27:  this.hide(); break; //Esc
                }

                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }
        });
    }
}
class ViewMain extends BaseView{
    init(){
        super.init();

        /**
         * Открыты другие модальные окна?
         * @type {object} idModal => bool (true - открыто, false - скрыто)
         */
        this.isVisibleModals = {};

        this.intervalUpdatingBard = 0; //интервал автоматического обновления баров (нужно лишь удерживать ползунок на определённой скорости) 

        this.oldValueOfSlider = 0;

        this.ids = {
            mainChart: this.idEl + '__main-chart',
            tradePanel: this.idEl + '__trade-panel',
            labelBalance: this.idEl + '__label-balance',
            labelCountAssets: this.idEl + '__label-count-assets',
            labelIncome: this.idEl + '__label-income',
            labelPrice: this.idEl + '__label-price',
            labelPriceChange: this.idEl + '__label-price-change',
            labelSumOperation: this.idEl + '__label-sum-operation',
            inputCountOperation: this.idEl + '__input-count-operation',
            buttonBuy: this.idEl + '__button-buy',
            buttonSell: this.idEl + '__button-sell',
            buttonNext: this.idEl + '__button-next',
            buttonNext10: this.idEl + '__button-next10',
            buttonNext30: this.idEl + '__button-next30',
            slider: this.idEl + '__slider',
            sliderLabel: this.idEl + '__slider-label',
            tableLast5Operations: this.idEl + '__table-last-5-operations',

            tableHeader: this.idEl + '__table-header',
        };

        /**
         * Знак денег
         * @type {string}
         */
        this.symbol = '$';

        /**
         * Текущая цена актива
         * @type {number}
         * @private
         */
        this._price = 0;

        /**
         * Класс отвечающий за игровой процесс
         * @type {Game|null}
         */
        this.game = null;

        this.clickBalance = this.clickBalance.bind(this);
        this.clickCountAssets = this.clickCountAssets.bind(this);

        this.buttonBuyClick = this.buttonBuyClick.bind(this);
        this.buttonSellClick = this.buttonSellClick.bind(this);
        this.buttonNextClick = this.buttonNextClick.bind(this);
        this.buttonNext10Click = this.buttonNext10Click.bind(this);
        this.buttonNext30Click = this.buttonNext30Click.bind(this);
        this.updateSumOperation = this.updateSumOperation.bind(this);
    }


    show(){
        this.el.show();

        //bootstrap style (moore beautiful)
        this.buttonToBootstrap(this.ids.buttonBuy, 'success');
        this.buttonToBootstrap(this.ids.buttonSell, 'danger');
        this.buttonToBootstrap(this.ids.buttonNext, 'primary');

        //set id
        $($$(this.ids.mainChart).getNode()).attr('id', this.ids.mainChart);

        //game
        this.game = Game.loadLastGame();
        if(this.game === null){
            ModalSelectStock.instance.show();
        }

        this.updateSumOperation();
        $($$(this.ids.inputCountOperation).getNode()).keyup(this.updateSumOperation);
        $($$(this.ids.inputCountOperation).getNode()).change(this.updateSumOperation);
    }

    key(){
        $(document).on('keydown', (event) => {
            if(!Object.values(this.isVisibleModals).filter(x => x).length) {

                switch (event.keyCode) {
                    case 27: ModalMenu.instance.show(); break; //ESC
                    case 39: this.buttonNextClick(); break; //right arrow
                }

                if(event.keyCode === 27){
                    event.preventDefault();
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                }
            }
        });
    }

    //events
    clickBalance(){
        const maxSum = this.game.balance / this.game.price;
        $$(this.ids.inputCountOperation).setValue(maxSum);
        this.updateSumOperation();
    }
    clickCountAssets(){
        $$(this.ids.inputCountOperation).setValue(Math.abs(this.game.countPositions));
        this.updateSumOperation();
    }


    //*** Business Logic ***//
    set balance(value){
        $$(this.ids.labelBalance).setValue(this.roundToPrice(value) + this.symbol);
    }

    set countPositions(value){
        $$(this.ids.labelCountAssets).setValue(value);
    }

    set income(value){
        $$(this.ids.labelIncome).setValue(this.roundToPrice(value) + this.symbol);

        //color
        webix.html.removeCss($$(this.ids.labelIncome).getNode(), 'redText');
        webix.html.removeCss($$(this.ids.labelIncome).getNode(), 'greenText');
        if(value > 0){
            webix.html.addCss($$(this.ids.labelIncome).getNode(), 'greenText');
        }
        else if(value < 0){
            webix.html.addCss($$(this.ids.labelIncome).getNode(), 'redText');
        }
    }

    set price(value){
        $$(this.ids.labelPrice).setValue(value + this.symbol);
        this.priceChange = value - this._price;
        this._price = value;
        this.updateSumOperation()
    }

    set priceChange(value){
        $$(this.ids.labelPriceChange).setValue(this.roundToPrice(value) + this.symbol);
        const elLabelPriceChange = $$(this.ids.labelPriceChange).getNode();

        //color change price
        webix.html.removeCss(elLabelPriceChange, 'redText');
        webix.html.removeCss(elLabelPriceChange, 'greenText');
        if(value !== 0){
            webix.html.addCss(elLabelPriceChange, value > 0 ? 'greenText' : 'redText');
        }
    }

    set history5lastOperations(value){
        $$(this.ids.tableLast5Operations).clearAll();
        $$(this.ids.tableLast5Operations).parse(value.slice(0, 5));
        $$(this.ids.tableLast5Operations).refresh();
    }

    get inputCount(){
        return parseFloat($$(this.ids.inputCountOperation).getValue());
    }
    set inputCount(value){
        $$(this.ids.inputCountOperation).setValue(value);
    }

    buttonBuyClick(){}
    buttonSellClick(){}
    buttonNextClick(){}
    buttonNext10Click(){}
    buttonNext30Click(){}


    /**
     * Обновление показываемой суммы long/short/sell
     */
    updateSumOperation(){
        if(this.game){
            let value = parseFloat($$(this.ids.inputCountOperation).getValue());
            value *= this.game.price;
            $$(this.ids.labelSumOperation).setValue(this.roundToPrice(value || 0) + '$');
            $$(this.ids.labelSumOperation).refresh();
        }
    }

    buttonToBootstrap (id, type) {
        const el = $($$(id).getNode());

        el.removeClass();
        el.find('div').removeClass();
        el.find('button').removeClass();
        el.find('button').addClass('btn btn-' + type);
    }

    /**
     * Показ сообщения
     * @param {string} text
     */
    showMessage(text){
        ModalAlert.instance.show(text);
    }

    /**
     * Показ сообщения об окончании данных
     * @param {string} text
     */
    showEndMessage(text){
        ModalEndAlert.instance.show(text);
    }

    /**
     * Откругляет число до такого же количества знаков, что и в цене
     * @param number
     */
    roundToPrice(number){
        return Helper.roundToPrice(number, this._price);
    }

    clear(){
        this.game = new Game();
        this.game.clear();
    }
}
ViewMain.instance = new ViewMain();

webix.ready(() => {
    const model = ViewMain.instance;

    webix.ui({
        type:'line',
        id: model.idEl,
        cols: [
            { view: 'list', id: model.ids.mainChart },
            {
                width: 250,
                css: model.ids.tradePanel,
                padding: 15,
                rows: [
                    {
                        height: 30,
                        cols: [
                            {view: 'label', label: 'Баланс:', width: 65 },
                            {
                                view: 'label',
                                label: '0$',
                                id: model.ids.labelBalance,
                                css: 'pointer',
                                click: model.clickBalance
                            },
                            {width: 5 },
                            {view: 'button', type:'icon', icon:'bars', click: ModalMenu.instance.show, width: 25}
                        ]
                    },
                    {
                        height: 30,
                        cols: [
                            {view: 'label', label: 'Взято партий:', width: 110 },
                            {
                                view: 'label',
                                label: '0',
                                id: model.ids.labelCountAssets,
                                css: 'pointer',
                                click: model.clickCountAssets
                            }
                        ]
                    },
                    {
                        height: 30,
                        cols: [
                            {view: 'label', label: 'Доход:', width: 60 },
                            {view: 'label', label: '0$', id: model.ids.labelIncome},
                        ]
                    },
                    {
                        height: 30,
                        cols: [
                            { view: 'label', label: 'Текущая цена:', width: 110 },
                            { view: 'label', label: '0$', id: model.ids.labelPrice},
                        ]
                    },
                    {
                        height: 30,
                        cols: [
                            { view: 'label', label: 'Изменение за 1 бар:', width: 150 },
                            { view: 'label', label: '0$', id: model.ids.labelPriceChange},
                        ]
                    },
                    {},
                    {
                        cols: [
                            {
                                rows: [
                                    { view: 'button', value: 'Купить', id: model.ids.buttonBuy, click: () => model.buttonBuyClick() },
                                    { height: 20 },
                                    { view: 'button', value: 'Продать', id: model.ids.buttonSell, click: () => model.buttonSellClick() },
                                ]
                            },
                            {
                                rows: [
                                    {},
                                    {
                                        view: 'text',
                                        id: model.ids.inputCountOperation,
                                        attributes: { type: 'number', min: 1 },
                                        css: 'inputCount',
                                        value: '1',
                                    },
                                    { view: 'label', label: '0$', id: model.ids.labelSumOperation},
                                    {},
                                ]
                            },
                        ]
                    },
                    { height: 20 },
                    { view: 'button', value: 'Следующий ход', id: model.ids.buttonNext, click: () => model.buttonNextClick() },
                    { height: 10 },
                    { view: 'button', value: 'Следующие 10 ходов', id: model.ids.buttonNext10, click: () => model.buttonNext10Click() },
                    { height: 10 },
                    { view: 'button', value: 'Следующие 30 ходов', id: model.ids.buttonNext30, click: () => model.buttonNext30Click() },
                    { height: 10 },
                    { view: 'label', label: 'Пружина. баров в секунду: 0', id: model.ids.sliderLabel },
                    { view: 'slider', value: 0, max: 10, step: 0.1, id: model.ids.slider,
                        on:{
                            onChange:function(){
                                clearInterval(model.intervalUpdatingBard);
                                $$(model.ids.sliderLabel).setValue('Пружина. баров в секунду: 0');
                                this.setValue(0);
                                this.refresh();
                            },
                            onSliderDrag:function(){
                                var value = +this.getValue().toFixed(1);

                                if(model.oldValueOfSlider != value){
                                    model.oldValueOfSlider = value;

                                    $$(model.ids.sliderLabel).setValue('Пружина. баров в секунду: ' + value);

                                    clearInterval(model.intervalUpdatingBard);
                                    if(value > 0){
                                        //model.buttonNextClick();
                                        model.intervalUpdatingBard = setInterval(model.buttonNextClick, 1000 / value);
                                    }
                                }
                            }
                        }},
                    {},
                    { height: 10 },
                    { view: 'label', label: 'Последние 5 операций', css: model.ids.tableHeader },
                    {
                        view: 'datatable',
                        id: model.ids.tableLast5Operations,
                        select: true,
                        scroll: false,
                        css:  model.ids.tableLast5Operations,
                        rowHeight: 20,
                        columns: [
                            { id:'operationType',   width: 65,   header: {text: 'Операция', height: 25}   },
                            { id:'price',           width: 50,   header:'Цена'       },
                            { id:'countPositions',  width: 60,   header:'Позиций'    },
                            { id:'incomeShow',      width: 45,   header:'Доход'      },
                        ],
                        on:{
                            'onAfterRender': function() {
                                this.data.each((obj, i) => {
                                    let css = '';
                                    if(obj.income > 0){
                                        css = 'greenText';
                                    }
                                    else if(obj.income < 0) {
                                        css = 'redText';
                                    }
                                    obj.incomeShow = model.roundToPrice(obj.income) + model.symbol;
                                    if(css){
                                        $(this.getNode()).find('[aria-rowindex="' + (i + 1) + '"]').addClass(css);
                                    }
                                });
                            },
                        },
                    }
                ]
            },
        ]
    });

    model.show();
});

class WidgetModal{
    static template(modelView, title, width, height, left, top, body, resize, onResize){
        return {
            view: 'window',
            modal: true,
            id: modelView.idEl,
            width: width || 900,
            height: height || 550,
            left: left || 250,
            top: top || 100,
            move: true,
            resize: resize || false,
            head: {
                height: 35,
                type: 'line',
                cols: [
                    {
                        cols: [
                            {width: 5 },
                            {view: 'label', label: title}
                        ]
                    },
                    {view: 'button', label: 'x', width: 30, click: modelView.hide}
                ]
            },
            body: body,
            on: {
                onViewResize: () => {
                    if(onResize && typeof onResize === 'function'){
                        onResize();
                    }
                }
            }
        };
    }
}
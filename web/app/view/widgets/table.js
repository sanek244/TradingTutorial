class WidgetTable{
    static template(idTable, classCss, columns, onSelectChange, onAfterLoad, scroll = 'xy', onAfterRender, onAfterScroll) {
        let onAfterRenderSettings = false;

        return {
            view: 'datatable',
            id: idTable,
            css: 'table ' + classCss,
            select: true,
            scroll: scroll,
            navigation: true,
            resizeColumn: true,
            headermenu: true,
            columns: columns,
            dragColumn: true,
            on: {
                'data->onStoreUpdated': function () {
                    //index
                    this.data.each(function (obj, i) {
                        obj.index = i + 1;
                    })
                },
                'onColumnResize': (idColumn, newWidth, oldWidth, user_action) => {
                    //save
                    if (user_action) {
                        let localeData = localStorage.getItem(idTable);
                        if (localeData === 'undefined') {
                            localeData = '{}';
                        }

                        const table = JSON.parse(localeData) || {};
                        if (!table[idColumn]) {
                            table[idColumn] = {};
                        }
                        table[idColumn].width = newWidth;
                        localStorage.setItem(idTable, JSON.stringify(table));
                    }
                },
                'onAfterScroll': function () {
                    if (onAfterScroll) {
                        onAfterScroll(this);
                    }
                },
                'onAfterRender': function () {
                    onAfterRenderSettings = true;
                    if (localStorage.getItem(idTable)) {

                        //настройка столбцов
                        let localeData = localStorage.getItem(idTable);
                        if (localeData === 'undefined') {
                            localeData = '{}';
                        }
                        const table = JSON.parse(localeData) || {};


                        Object.keys(table).map(key => {
                            if (table[key].width && $$(idTable).isColumnVisible(key)) {
                                $$(idTable).setColumnWidth(key, table[key].width);
                            }
                            if (table[key].hidden) {
                                $$(idTable).hideColumn(key);
                            }
                            else {
                                $$(idTable).showColumn(key);
                            }
                            if (table[key].index) {
                                $$(idTable).moveColumn(key, table[key].index);
                            }
                        });
                    }

                    if (onAfterRender) {
                        onAfterRender(this);
                    }
                },
                'onSelectChange': () => {
                    //event
                    if (onSelectChange) {
                        const row = $$(idTable).getSelectedId(true)['0'];
                        const indexRow = row ? row.row : null;

                        if (indexRow !== null) {
                            onSelectChange($$(idTable).getItem(indexRow));
                        }
                    }
                },
                'onAfterColumnHide': idColumn => {
                    //save
                    if (onAfterRenderSettings) {
                        let localeData = localStorage.getItem(idTable);
                        if (localeData === 'undefined') {
                            localeData = '{}';
                        }

                        const table = JSON.parse(localeData) || {};
                        if (!table[idColumn]) {
                            table[idColumn] = {};
                        }
                        table[idColumn].hidden = true;
                        localStorage.setItem(idTable, JSON.stringify(table));
                    }
                },
                'onAfterColumnShow': idColumn => {
                    //save
                    if (onAfterRenderSettings) {
                        let localeData = localStorage.getItem(idTable);
                        if (localeData === 'undefined') {
                            localeData = '{}';
                        }

                        const table = JSON.parse(localeData) || {};
                        if (!table[idColumn]) {
                            table[idColumn] = {};
                        }
                        table[idColumn].hidden = false;
                        localStorage.setItem(idTable, JSON.stringify(table));
                    }
                },
                'onAfterSort': (idColumn, dir, as) => {
                    //save
                    let localeData = localStorage.getItem(idTable);
                    if (localeData === 'undefined') {
                        localeData = '{}';
                    }

                    const table = JSON.parse(localeData) || {};
                    if (!table[idColumn]) {
                        table[idColumn] = {};
                    }
                    table.sort = {by: idColumn, dir: dir, as: as};
                    localStorage.setItem(idTable, JSON.stringify(table));
                },
                'onAfterColumnDrop': (idColumn, idExtruded) => {
                    //save
                    let localeData = localStorage.getItem(idTable);
                    if (localeData === 'undefined') {
                        localeData = '{}';
                    }

                    const table = JSON.parse(localeData) || {};

                    $$(idTable).eachColumn(idColumn => {
                        if (!table[idColumn]) {
                            table[idColumn] = {};
                        }
                        table[idColumn].index = $$(idTable).getColumnIndex(idColumn);
                    });
                    localStorage.setItem(idTable, JSON.stringify(table));
                },
                'onAfterLoad': function () {
                    if (onAfterLoad) {
                        onAfterLoad.bind(this)();
                    }

                    //настройка столбцов
                    let localeData = localStorage.getItem(idTable);
                    if (localeData === 'undefined') {
                        localeData = '{}';
                    }
                    const table = JSON.parse(localeData) || {};

                    //sort
                    if (table.sort && typeof table.sort === 'object') {
                        $$(idTable).sort(table.sort.by, table.sort.dir, table.sort.as);
                    }
                },
            },
            data: [],
        };
    }
}
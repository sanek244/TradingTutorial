class WidgetModalFixed {
    static template(id, title, width, height, left, top, leftHeaderMargin, img, body, close, resize) {
        return {
            view: 'window',
            modal: true,
            id: id,
            width: width || 550,
            height: height || 300,
            left: left || 350,
            top: top || 200,
            move: true,
            resize: resize || false,
            head: {
                padding: 10,
                height: 60,
                type: 'line',
                css: 'noBorder',
                cols: [
                    {
                        cols: [
                            {width: leftHeaderMargin || 1},
                            {
                                view: 'template',
                                template: '<img src="./media/' + img + '.png">',
                                width: 40,
                                css: 'noBorder'
                            },
                            {width: 10},
                            {view: 'label', label: title, id: id + '__title', css: 'WidgetModalFixed__title'}
                        ]
                    },
                    {
                        rows: [
                            {
                                view: 'button',
                                css: 'button-close',
                                label: 'x',
                                width: 30,
                                click: () => {
                                    if (close && typeof close === 'function') {
                                        close();
                                    }
                                    else {
                                        $$(id).hide()
                                    }
                                },
                                height: 35
                            },
                            {height: 10},
                        ]
                    }
                ]
            },
            body: body
        };
    }
}
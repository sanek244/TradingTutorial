const fs = require('fs');
const glob = require('glob');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');

glob('./data/currency/*.csv', (err, files) => {
    if (err) {
        console.log('Error', err)
    } else {
        files.map(file => convert(file));
    }
});

const convert = file => {
    let nameCurrency = file.split('/').pop();
    nameCurrency = nameCurrency.substr(0, nameCurrency.length - 4);

    fs.readFile(file, 'utf-8', (err, data) => {
        if(err) throw err;

        data = data
            .split('\r')
            .slice(1)
            .map(line => {
                const arr = line.replace('\n', '').split(',');
                if(parseInt(arr[5]) === 0){
                    return null;
                }
                return arr;
            })
            .filter(x => x)
            .map(el => {
                let time = el[0];
                time = time[3] + time[4] + '.' + time[0] + time[1] + time.substring(5);
                return [(new Date(time)).getTime(), parseFloat(el[1]), parseFloat(el[2]), parseFloat(el[3]), parseFloat(el[4]), parseFloat(el[5])];
            }); //"Date","Open","High","Low","Close","Volume"


        Logic.convertAndSaves(TypeTimeFrame.DAY, data, nameCurrency, nameCurrency);
    });
};


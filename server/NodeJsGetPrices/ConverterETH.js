const fs = require('fs');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');

const nameFile = 'ETHUSD';
fs.readFile('data/' + nameFile + '.json', 'utf-8', (err, data) => {
    if(err) throw err;

    data = JSON.parse(data);
    data = data
        .map(el => [(new Date(el[0])).getTime(), el[1], el[3], el[4], el[2], el[5]]);  //"Date","Open","High","Low","Close","Volume"

    Logic.convertAndSaves(TypeTimeFrame.DAY, data, nameFile, nameFile);
});


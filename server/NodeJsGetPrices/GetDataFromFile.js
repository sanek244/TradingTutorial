const fs = require('fs');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');
const Additions = require('./Additions');
Additions.stringFormat();
Additions.replaceAll();

const name = 'AAPL';
fs.readFile('../data/temp/data.txt', 'utf-8', (err, data) => {
    if(err) throw err;

    data = JSON.parse(data);
	data['data']['chart'] = data['data']['chart']
		.map(el => {
			el = el['z'];
			return [(new Date(el['dateTime'])).getTime(), parseFloat(el['open']), parseFloat(el['high']), parseFloat(el['low']), parseFloat(el['close']), parseFloat(el['volume'].replaceAll(',', ''))];
		}); //"Date","Open","High","Low","Close","Volume"

	Logic.convertAndSaves(TypeTimeFrame.DAY, data['data']['chart'], name)
	console.log('Готово!');
});
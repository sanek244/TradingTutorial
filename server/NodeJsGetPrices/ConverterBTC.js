const fs = require('fs');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');

const nameFile = 'BTCUSD';
fs.readFile('data/' + nameFile + '.json', 'utf-8', (err, data) => {
    if(err) throw err;

    data = JSON.parse(data);
    data = data
        .map(el => [(new Date(el[1])).getTime(), el[3], el[5], el[6], el[4], el[7]]);  //"Date","Open","High","Low","Close","Volume"

    Logic.convertAndSaves(TypeTimeFrame.DAY, data, nameFile, nameFile);
});


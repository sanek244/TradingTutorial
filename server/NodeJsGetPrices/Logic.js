const fs = require('fs');
const Additions = require('./Additions');
Additions.stringFormat();

class Logic{

    /**
     * Добавляет среднеарифметическую скользящую
     */
    static addArithmeticMean(data){
        return data
            .sort((a, b) => {
                if(a[0] > b[0]){
                    return 1;
                }
                if(a[0] < b[0]){
                    return -1;
                }
                return 0;
            })
            .map((el, index, array) => {
            //среднеарифметическая скользящая
            let sum = 0,
                sum50 = 0,
                sum100 = 0,
                sum200 = 0;
            for(let i = index; i > index - 200; i--){
                sum += i > -1 ? array[i][4] : array[0][4];

                if(i === index - 50 + 1){
                    sum50 = sum;
                }
                if(i === index - 100 + 1){
                    sum100 = sum;
                }
            }
            sum200 = sum;

            //общая скользящая с весовыми коэффициентами
            let sumKof50 = 0,
                sumKof100 = 0,
                sumKof200 = 0;
            for(let i = index; i > index - 200; i--){
                const elValue = Math.pow(i > -1 ? array[i][4] : array[0][4], 2);
                if(i > index - 50){
                    sumKof50 += elValue / sum50;
                }
                if(i > index - 100){
                    sumKof100 += elValue / sum100;
                }
                sumKof200 += elValue / sum200;
            }

            let kofFixed = (el[1] + '').length - (el[1] + '').indexOf('.');
            el.push(parseFloat(sumKof50.toFixed(kofFixed)));
            el.push(parseFloat(sumKof100.toFixed(kofFixed)));
            el.push(parseFloat(sumKof200.toFixed(kofFixed)));

            return el;
        })
    }


    /**
     *
     * @param {string} timeFrame - TypeTime
     * @param {Array} data
     * @param {string} nameFile
     */
    static convertAndSaves(timeFrame, data, nameFile){
        data = Logic.addArithmeticMean(data);

        fs.writeFile('../data/{0}/{1}.json'.format(timeFrame, nameFile), JSON.stringify(data), (err) => {
            if(err) throw err;
        })
    }
}

module.exports = Logic;
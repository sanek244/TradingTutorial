const fs = require('fs');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');
const https = require('https');
const Additions = require('./Additions');
Additions.stringFormat();
Additions.replaceAll();

const name = 'AAPL';
const date = new Date();
const url = 'https://api.nasdaq.com/api/quote/{0}/chart?assetclass=stocks&fromdate=2000-01-01&todate={1}-{2}-{3}'
    .format(name, date.getFullYear(), date.getMonth() + 1, date.getDate());
https.get(url, (res) => {
    let dataAll = '';
    res.on('data', data => {
        dataAll += data
    });
    
    res.on('end', () => {
        data = JSON.parse(dataAll);
        data['data']['chart'] = data['data']['chart']
            .map(el => {
                el = el['z'];
                return [(new Date(el['dateTime'])).getTime(), parseFloat(el['open']), parseFloat(el['high']), parseFloat(el['low']), parseFloat(el['close']), parseFloat(el['volume'].replaceAll(',', ''))];
            }); //"Date","Open","High","Low","Close","Volume"

        Logic.convertAndSaves(TypeTimeFrame.DAY, data['data']['chart'], name)
    });
})
.on('error', error => {
    throw error
});


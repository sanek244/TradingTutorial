const fs = require('fs');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');

const nameFile = 'Apple';
fs.readFile('data/' + nameFile + '.json', 'utf-8', (err, data) => {
    if(err) throw err;

    data = JSON.parse(data);
    let isFoundError = false;
    let isFoundError2 = false;
    let isFoundError3 = false;
    let isFoundError4 = false;

    data['dataset']['data'] = data['dataset']['data']
        .map(el => {
            if(el[0] === '2014-06-06'){
                isFoundError = true;
            }
            if(el[0] === '2005-02-25'){
                isFoundError2 = true;
            }
            if(el[0] === '2000-06-20'){
                isFoundError3 = true;
            }
            if(el[0] === '1987-06-15'){
                isFoundError4 = true;
            }

            if(isFoundError){
                el[1] = parseFloat((el[1] / 7).toFixed(2));
                el[2] = parseFloat((el[2] / 7).toFixed(2));
                el[3] = parseFloat((el[3] / 7).toFixed(2));
                el[4] = parseFloat((el[4] / 7).toFixed(2));
                el[5] *= 7;
            }
            if(isFoundError2){
                el[1] = parseFloat((el[1] / 2).toFixed(2));
                el[2] = parseFloat((el[2] / 2).toFixed(2));
                el[3] = parseFloat((el[3] / 2).toFixed(2));
                el[4] = parseFloat((el[4] / 2).toFixed(2));
                el[5] *= 2;

                if(el[0] === '2000-06-21'){
                    el[5] *= 2;
                }
            }
            if(isFoundError3){
                el[1] = parseFloat((el[1] / 2).toFixed(2));
                el[2] = parseFloat((el[2] / 2).toFixed(2));
                el[3] = parseFloat((el[3] / 2).toFixed(2));
                el[4] = parseFloat((el[4] / 2).toFixed(2));
                el[5] *= 2;
                if(el[0] === '1987-06-16'){
                    el[5] *= 2;
                }
            }
            if(isFoundError4){
                el[1] = parseFloat((el[1] / 2).toFixed(2));
                el[2] = parseFloat((el[2] / 2).toFixed(2));
                el[3] = parseFloat((el[3] / 2).toFixed(2));
                el[4] = parseFloat((el[4] / 2).toFixed(2));
                el[5] *= 2;
            }
            return el;
        })
        .map(el => [(new Date(el[0])).getTime(), el[1], el[2], el[3], el[4], el[5]]); //"Date","Open","High","Low","Close","Volume"

    const paperName = data['dataset']['name'].substr(0, data['dataset']['name'].indexOf(')') + 1);
    Logic.convertAndSaves(TypeTimeFrame.DAY, data['dataset']['data'], nameFile, paperName)
});


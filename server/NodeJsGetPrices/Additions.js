class Additions{
    static stringFormat(){
        // Прокачка строки - функция format: 'Покупай машину {0} за {1} рублей'.format('mustang', 3100200);
        if (!String.prototype.format) {
            String.prototype.format = function() {
                const args = arguments;
                return this.replace(/{(\d+)}/g, (match, number) => {
                    if(args[number] === null){
                        return 'null';
                    }
                    if(typeof args[number] === 'undefined'){
                        return match;
                    }
                    if(typeof args[number] === 'object'){
                        return JSON.stringify(args[number]);
                    }
                    return args[number];
                });
            };
        }
    }

    static replaceAll(){
        if (!String.prototype.replaceAll) {
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.replace(new RegExp(search, 'g'), replacement);
            };
        }
    }
}

module.exports = Additions;
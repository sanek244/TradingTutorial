'use strict';

class TypeTimeFrame{ }

TypeTimeFrame.DAY = 'days';
TypeTimeFrame.HOUR = 'hours';
TypeTimeFrame.MINUTES_15 = 'minutes15';

TypeTimeFrame.labels = {
    [TypeTimeFrame.DAY]: 'Дневной',
    [TypeTimeFrame.HOUR]: 'Часовик',
    [TypeTimeFrame.MINUTES_15]: '15-ти минутка',
};

module.exports = TypeTimeFrame;
const fs = require('fs');
const readline = require('readline');
const glob = require('glob');
const Logic = require('./Logic');
const TypeTimeFrame = require('./TypeTimeFrame');

glob('./data/currency/15m/*.csv', (err, files) => {
    if (err) {
        console.log('Error', err)
    } else {
        files.map(file => convert(file));
    }
});

const convert = file => {
    let nameCurrency = file.split('/').pop();
    nameCurrency = nameCurrency.substr(0, nameCurrency.length - 4);

    const lineReader = readline.createInterface({
        input: fs.createReadStream(file)
    });

    let data = [];
    let high = 0,
        low = 0,
        open = 0,
        volume = 0,
        countBars = 0,
        dateFirstBar = 0;

    lineReader.on('line', line => {
        //"Date","Open","High","Low","Close","Volume"
        const lineParts = line.split(',');

        if(lineParts[5] !== '0' && lineParts[4] !== 'Close'){
            lineParts[2] = parseFloat(lineParts[2]);
            lineParts[3] = parseFloat(lineParts[3]);

            //объединение последних 15 минуток в 1
            if(countBars === 0){
                let time = lineParts[0];
                time = time[3] + time[4] + '.' + time[0] + time[1] + time.substring(5);
                dateFirstBar = (new Date(time)).getTime();
                open = parseFloat(lineParts[1]);
            }

            if(high < lineParts[2]){
                high = lineParts[2];
            }
            if(low > lineParts[3] || low === 0){
                low = lineParts[3];
            }

            volume += parseFloat(lineParts[5]);

            countBars++;

            if(countBars === 15){
                data.push([dateFirstBar, open, high, low, parseFloat(lineParts[4]), volume]);
                countBars = high = low = open = volume = 0;
            }
        }
    });

    lineReader.on('close', () => {
        Logic.convertAndSaves(TypeTimeFrame.MINUTES_15, data, nameCurrency, nameCurrency);
    });
};


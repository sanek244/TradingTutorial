const express = require('express');
const fs = require('fs');
const glob = require('glob');
const bodyParser = require('body-parser');
const path = require('path');

/**
 * Название акции, бумаги, валюты со значениями таймфреймов и названия файла
 * { stockName: {days: './data/days/AUDUSD_sada.js', ...}
 * @type {{}}
 */
const namesWithPath = {};
/**
 * Название акции, бумаги, валюты со значениями таймфреймов и названия файла
 * { stockName: [days, hours, ...], ... }
 * @type {{}}
 */
const names = {};
const dirname = __dirname.replace('server', '');

glob(__dirname + '/data/*/*.*', (err, files) => {
    if (err) {
        console.log('Error', err)
    } else {
        files.map(file => {
            //get name
            const filePathParts = file.split('/');
            let name = filePathParts.pop();
            const timeFrame = filePathParts.pop();

            let indexEnd = name.indexOf('_');
            if(indexEnd === -1){
                indexEnd = name.indexOf('.');
            }
            name = name.substr(0, indexEnd);

            //create
            if(!(name in names)){
                names[name] = [];
                namesWithPath[name] = {};
            }

            //add
            names[name].push(timeFrame);
            namesWithPath[name][timeFrame] = file;
        });
    }
});

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
    next();
});

app.use(express.static(dirname + 'web'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/', (req, res) => {
    res.sendFile(dirname + 'web/index.html');
});

app.get('/api/v1/Data/Names', function(req, res) {
    res.send(JSON.stringify(names));
});


app.get('/api/v1/Data/Stock', function(req, res) {
	if(!namesWithPath[req.query.stockName] || !namesWithPath[req.query.stockName][req.query.timeFrame]){
		return res.send([]);
	}
	
    fs.readFile(
        namesWithPath[req.query.stockName][req.query.timeFrame],
        'utf-8',
        (err, data) => {
            if (err) throw err;
            res.send(data);
        });
});

app.listen(7004, function () {
    console.log('Start server http://localhost:7004/')
});